import { GuildMember } from 'discord.js';
import {brain, ensureBrainKeyExistsPromise} from './brain.mjs';

// typist
const canBackfireRoleID = process.env.BACKFIRE_ROLE_ID;

export const WEATHER_PATTERNS = [
  // Rare 1 - 9%
  { id: 'clear_night', description: 'It\'s a clear night. Backfires are extremely rare.', chance: 0.03 },
  { id: 'sunny', description: 'It\'s a sunny day! Backfires are less likely.', chance: 0.05 },
  { id: 'rainbow', description: 'A rainbow is in the sky! Backfires are quite rare.', chance: 0.07 },
  { id: 'night', description: 'It\'s a starry night! Backfires are less common in the tranquil night.', chance: 0.08 },

  // Increased 10 - 19%
  { id: 'cloudy', description: 'It\'s a cloudy day. ☁ ☁ The chance of backfires is slightly increased.', chance: 0.10 },
  { id: 'drizzling', description: 'It\'s lightly drizzling. The calm before the storm makes backfires somewhat likely.', chance: 0.11 },
  { id: 'misty', description: 'A mysterious mist has rolled in, slightly increasing the chance of backfires.', chance: 0.12 },
  { id: 'overcast', description: 'It\'s an overcast day. The gloomy sky slightly increases the chance of backfires.', chance: 0.13 },
  { id: 'windy', description: 'The wind is howling! Backfires could blow in any direction.', chance: 0.14 },
  { id: 'frost', description: 'It\'s frosty out there! The crisp cold air makes backfires a bit more probable.', chance: 0.15 },
  { id: 'sunset', description: 'A beautiful sunset, but don\'t be fooled - backfires are slightly more likely.', chance: 0.16 },
  { id: 'snowy', description: 'It\'s snowing! The cold makes backfires a bit more probable.', chance: 0.17 },
  { id: 'humid', description: 'It\'s humid today. The muggy weather slightly increases the chance of backfires.', chance: 0.18 },
  { id: 'foggy', description: 'It\'s foggy! Visibility is low and backfires might just sneak up on you.', chance: 0.19 },

  // Moderate 20 - 29 %
  { id: 'drought', description: 'There\'s a drought! The dry weather makes backfires more probable.', chance: 0.20 },
  { id: 'blazing', description: 'It\'s a blazing day! The heat makes backfires more common.', chance: 0.22 },
  { id: 'heatwave', description: 'Heatwave! This scorching weather increases backfire chances.', chance: 0.24 },
  { id: 'rainy', description: 'It\'s pouring rain! Backfires are more likely.', chance: 0.26 },
  { id: 'thunderstorm', description: '⛈ Thunderstorm warning! Backfires are quite likely!', chance: 0.28 },
  { id: 'stormy', description: 'There\'s a storm brewing! Backfires are highly likely.', chance: 0.29 },

  // Extreme 30%+
  { id: 'hailing', description: 'It\'s hailing! The unpredictable weather makes backfires more likely.', chance: 0.30 },
  { id: 'tornado', description: 'Tornado warning! Backfires are extremely likely!', chance: 0.39 },
  { id: 'hurricane', description: 'There\'s a hurricane! Backfires are almost certain!', chance: 0.40 },
  { id: 'catsdogs', description: 'It\'s raining cats and dogs! No literally! The landscape is covered in cats and dogs. They all want you to feed them. Backfires are pretty likely!', chance: 0.50 },
  { id: 'fire_tornado', description: 'There\'s a FIRE TORNADO! Run! Backfires are extremely likely!', chance: 0.70 },
  { id: 'blackness', description: 'I can\'t even see out the window. It\'s all dark like the air is made of tar. Backfires almost assured!', chance: 0.90 },
];


export async function scheduleWeatherChange() {
  // Choose a random time for the next weather change
  const nextChangeTime = getRandomTime();
  setTimeout(async () => {
    brain.data.weather = getRandomWeather();
    brain.data.weather.report = null;
    await brain.write();
    await scheduleWeatherChange();
  }, nextChangeTime);
}

function getRandomTime() {
  // between 30m and 3h

  // 30 minutes = 30 * 60 seconds = 30 * 60 * 1000 milliseconds
  const milliseconds = 30 * 60 * 1000;

  // Get a random number between 1 and 6
  const random = Math.floor(Math.random() * 6) + 1;

  // Convert the random hour to milliseconds
  return random * milliseconds;
}

export function getRandomWeather() {
  const randomIndex = Math.floor(Math.random() * WEATHER_PATTERNS.length);
  return WEATHER_PATTERNS[randomIndex];
}

export async function backfires(guildMember, setOn) {
  if (guildMember.id === setOn.id) {
    return false;
  }
  if (await canBackfire(guildMember)) {
    const weather = brain.data.weather;
    return Math.random() < weather.chance;
  }
}

export async function canBackfire(guildMember) {
  if (!(guildMember instanceof GuildMember)) {
    throw new TypeError('To do role things I need a guildMember');
  }

  await ensureBrainKeyExistsPromise('backfire_opt_in');
  const opt = brain.data.backfire_opt_in[guildMember.id];
  if (opt !== undefined) {
    return true;
  }
  // check to see if guildMember has the role that also allows backfiring
  return guildMember.roles.cache.has(canBackfireRoleID);
}

export async function hasBackfirableRole(guildMember) {
  if (!(guildMember instanceof GuildMember)) {
    throw new TypeError('To do role things I need a guildMember');
  }
  // check to see if guildMember has the role that also allows backfiring
  return guildMember.roles.cache.has(canBackfireRoleID);
}

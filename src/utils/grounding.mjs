
import { initBrain, brain, ensureBrainKeyExists } from './brain.mjs';
import { EmbedBuilder } from 'discord.js';
import { removeRoleByName } from './users.mjs';
const grounded_role = process.env.GROUNDED_ROLE;

// Temporary array for grounding objects
let groundingTemp = {};

/**
 * Utilities to help make grounding code happen in more than one place
 * @type {Object}
 */
export const groundingUtils = {
  testMinutes: function(minutes) {
    // test okay if minutes are less than 12
    return minutes <= 12;
  },

  /**
   * Removes grounding role from a user and clears their brain object
   * @param user_id string
   * @param guild   Guild
   */
  clearGrounding: async function(user_id, guild) {
    await initBrain();
    // remove the grounded role
    await removeRoleByName(user_id, guild, grounded_role);

    // remove the grounding from the brain if it exists
    if (brain.data.grounding[user_id]) {
      delete brain.data.grounding[user_id];
      await brain.write();
    }
  },

  isUserAlreadyGrounded: function(user_id) {
    ensureBrainKeyExists('grounding');
    return brain.data.grounding[user_id];
  },

  groundUser: async function(user_id, minutes_todo, minutes_minimum, percentage, instructions, secret = 'normal') {
    // make sure user has a grounding key in the brain
    ensureBrainKeyExists('grounding', user_id);

    // clean up data
    minutes_todo = Math.abs(minutes_todo);
    minutes_minimum = Math.abs(minutes_minimum);
    percentage = Math.abs(percentage);
    if (percentage > 100) {
      percentage = 100;
    }

    // Store grounding object in temporary array
    // This is a COPY for later comparison
    groundingTemp[user_id] = JSON.parse(JSON.stringify(brain.data.grounding[user_id]));

    if (brain.data.grounding[user_id].minutes_todo) {
      await groundingUtils.increaseGrounding(user_id, minutes_todo, minutes_minimum, percentage, secret);
    } else if (minutes_todo) {
      await groundingUtils.newGrounding(user_id, minutes_todo, minutes_minimum, percentage, secret);
    }
    const embed = this.createGroundingEmbed(user_id);
    // Clear temporary array AFTER getting the embed
    delete groundingTemp[user_id];
    return embed;
  },

  newGrounding: async function(user_id, minutes_todo, minutes_minimum, percentage, secret = 'normal') {
    // make sure user has a grounding key in the brain
    ensureBrainKeyExists('grounding', user_id);
    if (minutes_todo) {
      // received minutes so is going to set the grounding
      await groundingUtils.calculateGroundingData(user_id, minutes_todo, minutes_minimum, percentage, secret);
    } else {
      // Bot was unable to figure out the grounding, so is just gonna do something small
      await groundingUtils.calculateGroundingData(user_id, 1, 0, 0, secret);
    }
  },

  calculateGroundingData: async function(user_id, minutes_todo, minutes_minimum, percentage, secret = 'normal') {
    ensureBrainKeyExists('grounding', user_id);
    brain.data.grounding[user_id] = {
      'minutes_todo': minutes_todo,
      'minutes_min': minutes_minimum || 0,
      'minutes_completed': 0,
      'minimum_percentage': percentage || 0,
      'secret': secret,
    };
    await brain.write();
  },

  /**
   * Creates an embed using the EmbedBuilder by comparing the previous grounding object in groundingTemp and the current grounding object in brain.data.grounding based on the user ID
   * @param {string} user_id
   * @returns {Embed} Embed with updated grounding information
   */
  createGroundingEmbed: function(user_id) {
    const previousGrounding = groundingTemp[user_id];
    const currentGrounding = brain.data.grounding[user_id];

    if (!currentGrounding) {
      throw new Error('User not found in grounding data.');
    }

    const embed = new EmbedBuilder()
      .setTitle('You\'re Grounded!')
      .setDescription(`<@${user_id}> has been grounded!`)
      .setColor('#0099ff')
      .setFooter({text: 'Type `/ground_me` or `/surprise_me` to serve out your grounding time using Grounding Bot.'});

    if (currentGrounding.secret === 'secret' || previousGrounding.secret === 'secret') {
      embed.addFields({
        name: ':shushing_face: SECRET GROUNDING! :shushing_face:',
        value: 'The details of this grounding are a secret. Head to your grounding area and good luck!',
      });
      return embed;
    }

    if (previousGrounding && previousGrounding.minutes_todo !== currentGrounding.minutes_todo) {
      embed.addFields({
        name: 'Minutes to Complete',
        value: `Was: ${previousGrounding.minutes_todo ? previousGrounding.minutes_todo : 0} minutes\nNow: ${currentGrounding.minutes_todo} minutes`,
      });
    } else {
      embed.addFields({
        name: 'Minutes to Complete',
        value: `${currentGrounding.minutes_todo} minutes`,
      });
    }

    if (previousGrounding && previousGrounding.minutes_min !== currentGrounding.minutes_min) {
      embed.addFields({
        name: 'Minimum Grounding Session',
        value: `Was: ${previousGrounding.minutes_min || '1 '} minutes\nNow: ${
            currentGrounding.minutes_min || '1 '
          } minutes`,
      });
    } else {
      embed.addFields({
        name: 'Minimum Grounding Session',
        value: `${currentGrounding.minutes_min || '1 '} minutes`,
      });
    }

    if (previousGrounding && previousGrounding.minimum_percentage !== currentGrounding.minimum_percentage) {
      embed.addFields({
        name: 'Percentage Minimum',
        value: `Was: ${
            previousGrounding.minimum_percentage || '0'
          }%\nNow: ${currentGrounding.minimum_percentage || '0'}%`,
      });
    } else {
      embed.addFields({
        name: 'Percentage Minimum',
        value: `${currentGrounding.minimum_percentage || '0'}%`,
      });
    }

    return embed;
  },


  increaseGrounding: async function(user_id, minutes_todo, minutes_minimum, percentage, secret = 'normal') {
    // make sure user has a grounding key in the brain
    ensureBrainKeyExists('grounding', user_id);

    if (secret === 'secret') {
      // make an existing grounding secret
      brain.data.grounding[user_id].secret = secret;
    }

    if (minutes_todo) {
      brain.data.grounding[user_id].minutes_todo = +brain.data.grounding[user_id].minutes_todo + +minutes_todo;
    }

    if (minutes_minimum && minutes_minimum > brain.data.grounding[user_id].minutes_min) {
      brain.data.grounding[user_id].minutes_min = minutes_minimum;
    }

    if (percentage && percentage > brain.data.grounding[user_id].minimum_percentage) {
      brain.data.grounding[user_id].minimum_percentage = percentage;
    }

    await brain.write();
  },
}

/**
 * This is a place I'm keeping messages so that they can be
 * updated in one place because they're going to be used in
 * more than one place
 */
export const groundingMessages = {
  minMinuteMessage: 'Sorry, the minimum amount of minutes to '
      + 'do at a time has to be 12 or less. '
      + 'The bot can\'t do more than 12 minutes at a time.',


  howTo: '',

  statusEmbed: function (user_id, to_do = null, minutes_min = null, minimum_percentage = null) {
    ensureBrainKeyExists('grounding');
    const record = brain.data.grounding[user_id];
    if (!record) {
      return new EmbedBuilder()
        .setTitle('User not grounded')
        .setDescription(`The user with ID ${user_id} is not currently grounded.`)
        .setColor('#ff0000')
        .build();
    }

    const left = record.minutes_todo - record.minutes_completed;
    const left_msg = left === record.minutes_todo ? '' : `${left} minutes left.`;

    return new EmbedBuilder()
      .setTitle('User Grounding Status')
      .setDescription(`<@${user_id}> is currently grounded.`)
      .addField('Grounded for', `${record.minutes_todo} minutes`)
      .addField('Minimum grounding session', `${record.minutes_min ? record.minutes_min : 'none'} minutes`)
      .addField('Minimum percentage', `${record.minimum_percentage ? record.minimum_percentage + '%' : 'none'}`)
      .addField('Minutes completed', `${record.minutes_completed ? record.minutes_completed : '0'} minutes`)
      .addField('Minutes left', left_msg)
      .setColor('#0099ff')
      .build();
  },

  /**
     * Given a user ID, builds a status message for a user's grounding
     * @param user_id string
     */
  statusMessage: function(
    user_id
  ) {
    ensureBrainKeyExists('grounding');
    const record = brain.data.grounding[user_id];
    if (!record) {
      return `<@${user_id}> is not currently grounded.`;
    }

    if (record.secret === 'secret') {
      return `<@${user_id}> is grounded, but the settings are a secret! :shushing_face:`;
    }

    let grounded_for = `<@${user_id}> is grounded for ${record.minutes_todo} minutes, `;
    let minimum = record.minutes_min ?
        `with a minimum grounding session of ${record.minutes_min} minutes, ` :
      'with no minimum grounding session, ';
    let percentage = record.minimum_percentage ?
        `and a minimum percentage of ${record.minimum_percentage}%.\n` :
      'and no minimum percentage.\n';
    let completed = record.minutes_completed ?
        `${record.minutes_completed} minutes have been completed.\n` :
        `Nothing has been completed yet so there's still ${record.minutes_todo} minutes to do.`;

    const left = record.minutes_todo - record.minutes_completed;
    let left_msg = left === record.minutes_todo ? '' : `${left} minutes left.`;

    return grounded_for + minimum + percentage + completed + left_msg;
  }
}

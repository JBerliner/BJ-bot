import {brain, ensureBrainKeyExistsPromise} from './brain.mjs';

export const track = {
  updateShapeCountTracking: async function(user_id, isSuccess, timeTaken, difficulty, size, movement, color, guesses) {
    let trackingData = await ensureBrainKeyExistsPromise('tracking', 'shape_count', user_id);

    // Initialize or update specific stats
    trackingData.all = trackingData.all || this.createNewShapeStatsObject();
    trackingData.difficultyStats = trackingData.difficultyStats || {
      easy: this.createNewShapeStatsObject(),
      medium: this.createNewShapeStatsObject(),
      hard: this.createNewShapeStatsObject()
    };
    trackingData.sizeStats = trackingData.sizeStats || {
      x_small: this.createNewShapeStatsObject(),
      small: this.createNewShapeStatsObject(),
      medium: this.createNewShapeStatsObject(),
      large: this.createNewShapeStatsObject()
    };
    trackingData.movementStats = trackingData.movementStats || {
      still: this.createNewShapeStatsObject(),
      fade: this.createNewShapeStatsObject()
    };
    trackingData.colorStats = trackingData.colorStats || {
      random: this.createNewShapeStatsObject(),
      black: this.createNewShapeStatsObject(),
      white: this.createNewShapeStatsObject(),
      rainbow: this.createNewShapeStatsObject(),
      pastel: this.createNewShapeStatsObject()
    };

    // in case new options were added that aren't tracked above create new objects on keys
    trackingData.difficultyStats[difficulty] = trackingData.difficultyStats[difficulty] || this.createNewShapeStatsObject();
    trackingData.sizeStats[size] = trackingData.sizeStats[size] || this.createNewShapeStatsObject();
    trackingData.movementStats[movement] = trackingData.movementStats[movement] || this.createNewShapeStatsObject()
    trackingData.colorStats[color] = trackingData.colorStats[color] || this.createNewShapeStatsObject()

    // Use the helper function to update stats
    this.updateShapeStatsObject(trackingData.all, isSuccess, timeTaken, guesses);
    this.updateShapeStatsObject(trackingData.difficultyStats[difficulty], isSuccess, timeTaken, guesses);
    this.updateShapeStatsObject(trackingData.sizeStats[size], isSuccess, timeTaken, guesses);
    this.updateShapeStatsObject(trackingData.movementStats[movement], isSuccess, timeTaken, guesses);
    this.updateShapeStatsObject(trackingData.colorStats[color], isSuccess, timeTaken, guesses);

    await brain.write();
  },

  updateShapeStatsObject: function(statsObject, isSuccess, timeTaken, guesses) {
    statsObject.totalGuesses = (statsObject.totalGuesses || 0) + guesses;
    if (isSuccess) {
      statsObject.successfulGuesses = (statsObject.successfulGuesses || 0) + 1;
      if (timeTaken !== null) {
        statsObject.timeTaken = (statsObject.timeTaken || 0) + timeTaken;
        statsObject.averageTime = statsObject.timeTaken / statsObject.successfulGuesses;
      }
    } else {
      statsObject.failedGuesses = (statsObject.failedGuesses || 0) + 1;
    }
  },

  createNewShapeStatsObject: function() {
    return {
      totalGuesses: 0,
      successfulGuesses: 0,
      failedGuesses: 0,
      timeTaken: 0,
      averageTime: 0
    }
  },

  setUpGroundingTracking: async function(user_id) {
    // Ensure the key exists and get the tracking data
    let grounding_tracking = await ensureBrainKeyExistsPromise('tracking', 'grounding', user_id);

    // Initialize the fields if they are undefined or null
    grounding_tracking.minutesCompleted = grounding_tracking.minutesCompleted || 0;
    grounding_tracking.correctClicks = grounding_tracking.correctClicks || 0;
    grounding_tracking.incorrectClicks = grounding_tracking.incorrectClicks || 0;
    grounding_tracking.misses = grounding_tracking.misses || 0;

    await brain.write();
    return grounding_tracking;
  },

  updateGroundingTracking: async function(user_id, data) {
    let trackingData = await this.setUpGroundingTracking(user_id);

    trackingData.minutesCompleted += data.minutesCompleted;
    trackingData.correctClicks += data.correctClicks;
    trackingData.incorrectClicks += data.incorrectClicks;
    trackingData.misses += data.missedClicks;

    await brain.write();
  },

  setUpMathTracking: async function(user_id) {
    let tracking = await ensureBrainKeyExistsPromise('tracking', 'math', user_id);
    // Ensure the operation object exists
    if (!tracking.operation) {
      tracking.operation = {
        'addition': {
          'started': 0,
          'completed': 0,
          'correct': 0,
          'incorrect': 0
        },
        'subtraction': {
          'started': 0,
          'completed': 0,
          'correct': 0,
          'incorrect': 0
        },
        'multiplication': {
          'started': 0,
          'completed': 0,
          'correct': 0,
          'incorrect': 0
        },
        'division': {
          'started': 0,
          'completed': 0,
          'correct': 0,
          'incorrect': 0
        },
        'hard_mode': {
          'started': 0,
          'completed': 0,
          'correct': 0,
          'incorrect': 0
        },
      };
    }
    if (!tracking.practice) {
      tracking.practice = {
        'started': 0,
        'cancelled': 0,
        'completed': 0,
        'problems_assigned': 0,
        'problems_completed': 0,
        'problems_correct': 0,
        'problems_incorrect': 0,
        'time': 0
      };
    }
    if (!tracking.test) {
      tracking.test = {
        'started': 0,
        'cancelled': 0,
        'completed': 0,
        'problems_assigned': 0,
        'problems_completed': 0,
        'problems_correct': 0,
        'problems_incorrect': 0,
        'time': 0
      };
    }
    if (!tracking.all) {
      tracking.all = {
        'started': 0,
        'cancelled': 0,
        'completed': 0,
        'problems_assigned': 0,
        'problems_completed': 0,
        'problems_correct': 0,
        'problems_incorrect': 0,
        'time': 0
      };
    }

    await brain.write();
    return tracking;
  }
}

import {initBrain, brain, ensureBrainKeyExists, ensureBrainKeyExistsPromise} from './brain.mjs';
import {ChannelType, PermissionFlagsBits } from 'discord.js';
import randomWords from 'random-words'
import {getGuildMemberByUserID} from './users.mjs';

const charMap = {
  ' ': '\u{200B} ',
  'a': 'а', // Cyrillic 'а'
  'c': 'с', // Cyrillic 'с'
  'e': 'е', // Cyrillic 'е'
  'i': 'і', // Ukrainian 'і'
  'o': 'ο', // Greek 'ο'
  // 'p': 'ρ', // Greek 'ρ' - Too obviously different
  'x': 'х', // Cyrillic 'х'
  'y': 'у', // Cyrillic 'у'
  'A': 'Α', // Greek uppercase 'Α'
  'B': 'Β', // Greek uppercase 'Β'
  'C': 'С', // Cyrillic uppercase 'С'
  'E': 'Ε', // Greek uppercase 'Ε'
  'H': 'Н', // Cyrillic uppercase 'Н'
  'I': 'І', // Ukrainian/Cyrillic 'І'
  'J': 'Ј', // Cyrillic uppercase 'Ј'
  'K': 'Κ', // Greek uppercase 'Κ'
  'M': 'Μ', // Greek uppercase 'Μ'
  'N': 'Ν', // Greek uppercase 'Ν'
  'O': 'О', // Cyrillic uppercase 'О'
  'P': 'Ρ', // Greek uppercase 'Ρ'
  'S': 'Ѕ', // Cyrillic uppercase 'Ѕ'
  'T': 'Τ', // Greek uppercase 'Τ'
  'X': 'Х', // Cyrillic uppercase 'Х'
  'Y': 'Υ', // Greek uppercase 'Υ'
  'Z': 'Ζ', // Greek uppercase 'Ζ'
};

/**
 * Utilities to help make line code happen in more than one place
 * @type {Object}
 */
export const lineUtils = {

  clearLines: async function(user_id) {
    await initBrain();

    if (brain.data.lines[user_id]) {
      delete brain.data.lines[user_id];
      await brain.write();
    }
  },

  assignLinesToUser: async function(user, sender_id, guild, lines_todo, line, cmd_channel, hide_number_completed, random_case, penalty_lines, spectator_setting) {
    const user_id = user.id;
    ensureBrainKeyExists('lines', user_id);
    if (lineUtils.isUserAssignedLines(user_id)) {
      return '\n**That user already has lines assigned to them. These lines have not been assigned.**'
    }
    if (!lines_todo) {
      return 'There are no lines do do. What happened?';
    }

    random_case = (random_case === null) ? 'normal' : random_case;

    const member = await getGuildMemberByUserID(guild, user_id);
    const newLineChannel = await lineUtils.createLineChannel(user_id, member.displayName, sender_id, guild, spectator_setting);
    brain.data.lines[user_id] = {
      'lines_todo': lines_todo,
      'lines_completed': 0,
      'mistakes': 0,
      'username': user.username,
      'mistake_on_line': 0,
      'random_words': (!line),
      'line': line,
      'channel': newLineChannel.id,
      'cmd_channel': cmd_channel,
      'set_by_id': sender_id,
      'start_time': Date.now(),
      'last_time': Date.now(),
      'current_cpm': 0,
      'has_cheated': false,
      'hide_number_completed': hide_number_completed,
      'started': false,
      'random_case': random_case,
      'penalty_lines': penalty_lines
    };
    await brain.write();

    const nextLine = await lineUtils.getNextLine(user_id);
    let reply_text = `<@${user_id}> you must write `;
    if (!line) {
      reply_text += `${lines_todo} random lines. Your first line is:\n\n`;
    } else {
      reply_text += `the following line ${lines_todo} times:\n\n`;
    }
    reply_text += `**\u{200B}${nextLine.displayLine}\u{200B}**\n\nYou can cancel the task by typing "cancel".`;
    newLineChannel.send(reply_text);
    return lineUtils.generateLineMessage(user_id);
  },

  isUserAssignedLines(user_id) {
    ensureBrainKeyExists('lines', user_id);
    return !!brain.data.lines[user_id].lines_todo;
  },

  getNextLine: async function(user_id) {
    const data = brain.data.lines[user_id];
    let toWrite = data.line;
    if (data.random_words === true) {
      toWrite = lineUtils.generateRandomLine(10, 13, 0.5);
    }
    if (data.random_case === 'random') {
      toWrite = lineUtils.randomize_case(toWrite, 0.5);
    } else if (data.randomize_case === 'buggy') {
      // not doing anything here because that's how "buggy" works
    }

    const displayLine = lineUtils.anticheatFilter(toWrite);

    brain.data.lines[user_id].line = toWrite;
    await brain.write();

    return {toWrite, displayLine}
  },

  anticheatFilter: function(line) {
    return '\u{200B}' + line.split('').map(c => charMap[c] || c).join('') + '\u{200B}';
  },

  checkForCheating: function(givenLine, cpm) {
    if (this.isCopyPasting(givenLine)) return true;
    return this.isTypingUnusuallyFast(cpm);
  },

  isCopyPasting(givenLine) {
    const cheatRegex = new RegExp(Object.values(charMap).join('|'), 'gu');
    const hiddenCharRegex = /\u{200B}/gu;
    return cheatRegex.test(givenLine) || hiddenCharRegex.test(givenLine);
  },

  isTypingUnusuallyFast(cpm) {
    return cpm > 1000;
  },

  calculateCPM: function(givenLine, lastTime) {
    const messageLength = givenLine.length;
    const currentTime = Date.now();
    const timeSinceLastLine = currentTime - lastTime;
    const minutesPassed = timeSinceLastLine / 60000;
    const calculatedCPM = messageLength / minutesPassed;
    // console.log("CPM: " + calculatedCPM);
    return calculatedCPM;
  },

  calculateFinalWPM: function(totalCPM, numLines) {
    return Math.round(((((totalCPM / numLines) / 5) + Number.EPSILON) * 100)) / 100;
  },

  generateLineMessage: function(user_id) {
    let linesData = brain.data.lines[user_id];
    let reply_message = '\n';

    let displayLine = lineUtils.anticheatFilter(linesData.line);

    reply_message += `Lines to complete: ${linesData.lines_todo}\n`;
    if (linesData.random_words) {
      reply_message += 'Line to write: Random!\n';
    } else {
      reply_message += `Line to write: \u{200B}${displayLine}\u{200B}\n`;
    }

    return reply_message + '\n\nA new channel has been created just for you to type your lines in.';
  },

  generateRandomLine: function(minWords, maxWords, commaChance) {
    const words = randomWords({min: minWords, max: maxWords});
    let randomSentence = words.join(' ');

    // Add commas randomly
    const randNum = Math.random();
    if (randNum < commaChance) {
      const randomIndex = Math.floor(Math.random() * (words.length - 1));
      randomSentence = randomSentence.replace(words[randomIndex], words[randomIndex] + ',');
    }

    randomSentence = randomSentence.charAt(0).toUpperCase() + randomSentence.slice(1);

    const shouldBeQuestion = Math.random();
    if (shouldBeQuestion < 0.3) {
      randomSentence += '?';
    } else {
      randomSentence += '.';
    }

    return randomSentence;
  },

  insertAtIndex: function(str, index, value) {
    return str.slice(0, index) + value + str.slice(index);
  },

  createLineChannel: async function(user_id, username, sender_id, guild, spectator = "off") {
    const everyoneRole = guild.roles.everyone.id;

    // Default permissionOverwrites
    let permissionOverwrites = [
      {type: 'role', id: everyoneRole, deny: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: user_id, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: sender_id, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: process.env.DISCORD_CLIENT_ID, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
    ];

    if (spectator === 'viewing' || spectator === 'free-talking') {
      const permissions = [PermissionFlagsBits.ViewChannel];
      if (spectator === 'free-talking') {
        permissions.push(PermissionFlagsBits.SendMessages);
      }
      permissionOverwrites.push(
        {type: 'role', id: process.env.REQUIRED_ROLE_1, allow: permissions},
        {type: 'role', id: process.env.REQUIRED_ROLE_2, allow: permissions},
        {type: 'role', id: process.env.REQUIRED_ROLE_3, allow: permissions},
      );
    }

    const channel = await guild.channels.create({
      name: username + '-lines',
      type: ChannelType.GuildText,
      permissionOverwrites: permissionOverwrites,
      parent: process.env.LINES_CATEGORY_ID,
    });
    return channel;
  },
  randomize_case: function (s, caps_probabilty) {
    let result = '';
    for (let i = 0; i < s.length; i++) {
      if (Math.random() < caps_probabilty) {
        result += s[i].toUpperCase();
      } else {
        result += s[i].toLowerCase();
      }
    }
    return result
  },

  addLinesToDo: async function(user_id, amount_to_increase) {
    await ensureBrainKeyExistsPromise('lines', user_id);
    brain.data.lines[user_id].lines_todo + amount_to_increase;
    await brain.write();
  }
}

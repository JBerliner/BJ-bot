import {brain, ensureBrainKeyExistsPromise} from './brain.mjs';
import {ChannelType, PermissionFlagsBits } from 'discord.js';
import {getGuildMemberByUserID} from './users.mjs';
import { evaluate } from 'mathjs';
import {track} from './track.mjs';

export const problemUtils = {

  clearProblems: async function(user_id) {
    if (brain.data.problems[user_id]) {
      delete brain.data.problems[user_id];
      await brain.write();
    }
  },

  assignProblemsToUser: async function(user, sender_id, guild, problems_todo, operation, spectator_setting, test_mode, cmd_channel) {
    return await this.assignProblemsToUserCommon(user, sender_id, guild, problems_todo, operation, spectator_setting, test_mode, cmd_channel, 'general');
  },

  assignTimesTablesToUser: async function(user, sender_id, guild, spectator_setting, test_mode, cmd_channel) {
    const problems_todo = 81;
    return await this.assignProblemsToUserCommon(user, sender_id, guild, problems_todo, 'multiplication', spectator_setting, test_mode, cmd_channel, 'times_table');
  },

  assignProblemsToUserCommon: async function(user, sender_id, guild, problems_todo, operation, spectator_setting, test_mode, cmd_channel, setting = 'general') {
    await ensureBrainKeyExistsPromise('problems', user.id);
    const tracking = await track.setUpMathTracking(user.id);

    if (brain.data.problems[user.id].problems_todo) {
      return '\n**That user already has problems assigned to them. These problems have not been assigned.**';
    }

    const member = await getGuildMemberByUserID(guild, user.id);
    const newProblemChannel = await problemUtils.createProblemChannel(user.id, member.displayName, sender_id, guild, spectator_setting);

    let timesTables = {};

    if (setting === 'times_table') {
      for (let i = 1; i <= 9; i++) {
        for (let j = 1; j <= 9; j++) {
          let ij = i + '_' + j;
          timesTables[ij] = `${i} * ${j}`;
        }
      }
      operation = 'multiplication';
    }

    let problem_reply = problemUtils.mathTestMessage(test_mode);
    problem_reply += `<@${user.id}> you must solve ${problems_todo} problems. You can turn this assignment in incomplete the task by typing "cancel".\n`;
    problem_reply += '**When you are ready to start, send "start"**';
    newProblemChannel.send(problem_reply);

    brain.data.problems[user.id] = {
      'problems_todo': problems_todo,
      'problems_completed': 0,
      'correct': 0,
      'mistakes': 0,
      'username': member.displayName,
      'mistake_on_problem': 0,
      'channel': newProblemChannel.id,
      'cmd_channel': cmd_channel,
      'set_by_id': sender_id,
      'set_time': Date.now(),
      'first_problem_start': null,
      'last_time': Date.now(),
      'started': false,
      'currentProblem': null,
      'currentAnswer': null,
      'currentOperation': operation,
      'operation': operation,
      'testing': test_mode ? 'test' : 'practice',
      'setting': setting
    };

    if (setting === 'times_table') {
      brain.data.problems[user.id].times_tables_problems_remaining = timesTables;
    }

    // track problems assigned
    tracking.all.problems_assigned = +tracking.all.problems_assigned + +problems_todo;
    tracking[brain.data.problems[user.id].testing].problems_assigned = +tracking[brain.data.problems[user.id].testing].problems_assigned + +problems_todo;

    await brain.write();

    let reply_message = (setting === 'times_table') ? 'Times Table Practice!\n' : `Problems to solve: ${problems_todo}\n`;
    reply_message += '\nA new channel has been created just for you to do your problems in.';

    return reply_message;
  },

  createProblemChannel: async function(user_id, username, sender_id, guild, spectator = 'off') {
    const everyoneRole = guild.roles.everyone.id;

    // Default permissionOverwrites
    let permissionOverwrites = [
      {type: 'role', id: everyoneRole, deny: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: user_id, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: sender_id, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: process.env.DISCORD_CLIENT_ID, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
    ];

    if (spectator === 'viewing' || spectator === 'free-talking') {
      const permissions = [PermissionFlagsBits.ViewChannel];
      if (spectator === 'free-talking') {
        permissions.push(PermissionFlagsBits.SendMessages);
      }
      permissionOverwrites.push(
        {type: 'role', id: process.env.REQUIRED_ROLE_1, allow: permissions},
        {type: 'role', id: process.env.REQUIRED_ROLE_2, allow: permissions},
        {type: 'role', id: process.env.REQUIRED_ROLE_3, allow: permissions},
      );
    }

    const channel = await guild.channels.create({
      name: username + '-problems',
      type: ChannelType.GuildText,
      permissionOverwrites,
      parent: process.env.PROBLEMS_CATEGORY_ID,
    });
    return channel;
  },

  mathTestMessage: function(test_mode) {
    if (test_mode) {
      return '# Time for a Math Test!\n' +
        '* You should complete this exam without reference to notes, books, or other resources. ' +
        'You should not use a calculator (one will not be necessary), ' +
        'but may use scrap paper to do calculations if you wish.\n' +
        '* **If you do use a calculator, make sure to fess up to it when your test is turned in.** ' +
        'Many students have needed additional lessons in long division.\n' +
        '* **Eyes on your own paper.** Looking at another person\'s paper is ' +
        'reason to assume cheating and your paper will be taken.\n' +
        '* All Cell phones and electronic devices must be OFF and put away and ' +
        'hats removed.\n' +
        '* **Make sure your answer is correct before submitting it.** ' +
        'You will not be able to edit your answers later.\n' +
        '* You will find out what answers you got correct or incorrect at the end of the test.\n' +
        '## :warning: Turning this in early (sending "cancel") will affect your overall grade!\n\n';
    }
    return '';
  },

  calculateLetterGrade: function(percentage) {
    if (percentage >= 90) {
      return 'A';
    } else if (percentage >= 80) {
      return 'B';
    } else if (percentage >= 70) {
      return 'C';
    } else if (percentage >= 60) {
      return 'D';
    } else {
      return 'F';
    }
  }
}

export async function getRandomTimesTableProblem(timesTablesRemaining) {
  const keys = Object.keys(timesTablesRemaining);

  if (keys.length === 0) {
    // Handle this situation as you see fit.
    throw new Error('No remaining problems.');
  }

  const randomIndex = Math.floor(Math.random() * keys.length);
  const selectedProblemStr = keys[randomIndex];
  const [i, j] = timesTablesRemaining[selectedProblemStr].split(' * ').map(Number);

  let problem = timesTablesRemaining[selectedProblemStr];
  // Remove the selected problem from the object
  delete timesTablesRemaining[selectedProblemStr];

  return {
    problem: problem,
    answer: i * j,
    operation: 'multiplication'
  };
}


export function generateNewProblem(selectedOperation = 'all') {
  let operation_choice = Math.floor(Math.random() * 4);

  if (selectedOperation !== 'all') {
    switch (selectedOperation) {
      case 'addition': operation_choice = 0; break;
      case 'subtraction': operation_choice = 1; break;
      case 'multiplication': operation_choice = 2; break;
      case 'division': operation_choice = 3; break;
      case 'hard_mode': operation_choice = 4; break;
    }
  }

  const num1 = Math.floor(Math.random() * 100) + 1;
  const num2 = Math.floor(Math.random() * 100) + 1;

  let problem = '';
  let answer = 0;
  let operation;

  switch (operation_choice) {
    case 0:
      // Addition
      problem = `${num1} + ${num2}`;
      answer = num1 + num2;
      operation = 'addition';
      break;
    case 1:
      // Subtraction
      problem = `${num1} - ${num2}`;
      answer = num1 - num2;
      operation = 'subtraction';
      break;
    case 2:
      // Multiplication
      problem = `${num1} * ${num2}`;
      answer = num1 * num2;
      operation = 'multiplication';
      break;
    case 3:
      // Division
      problem = `${num1} / ${num2}`;
      answer = Math.round(num1 / num2 * 100) / 100; // Rounding the answer to 2 decimal places
      operation = 'division';
      break;
    case 4:
      // Hard Mode
      let numInts = Math.floor(Math.random() * 3) + 3;
      let operations = ['+', '-', '*', '/']
      for (let i = 0; i < numInts; i++) {
        if (i != numInts - 1) {
          problem += `${(Math.floor(Math.random() * 100) + 1)} ${operations[Math.floor(Math.random() * 4)]} `;
        } else {
          problem += `${Math.floor(Math.random() * 100) + 1}`;
        }
      }
      answer = Math.round(evaluate(problem) * 100) / 100;
      operation = 'hard_mode';
      break;
    default:
      break;
  }

  return { problem, answer, operation };
}

/* eslint-env jest */
import { mockDeep } from 'jest-mock-extended';
import Discord, { Message, TextChannel } from 'discord.js';

export function getMessageMock() {
  const channelMock = mockDeep(TextChannel);
  const messageMock = mockDeep(Message);

  messageMock.channel = channelMock;

  return messageMock;
}

class MockGuild extends Discord.Guild {
  constructor() {
    const client = new Discord.Client({
      partials: [],
      intents: [],
    });
    super(client, {} /* data */);
    this.roles = {
      cache: {
        find: jest.fn(),
      },
    };
    this.members = {
      fetch: jest.fn(),
    };
  }
}

export function getGuildMock() {
  return new MockGuild();
}


export function getRoleMock() {
  return {
    id: '123456789',
    name: 'Role Name',
  };
}

export function getGuildMemberMock() {
  const member = {
    id: '123456789',
    roles: {
      cache: new Map(),
    },
  };

  member.roles.add = jest.fn().mockImplementation((role) => {
    member.roles.cache.set(role.id, role);
  });

  member.roles.remove = jest.fn().mockImplementation((role) => {
    member.roles.cache.delete(role.id);
  });

  return member;
}

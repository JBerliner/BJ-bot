import Discord from 'discord.js';
import {getGuildMemberByUserID, isUserInGuild} from './users.mjs';
import {brain, ensureBrainKeyExists} from './brain.mjs';

/**
 * Returns whether there's consent, given user objects
 * @param {Discord.User} top_user
 * @param {Discord.User} sub_user
 * @param {Discord.Guild} [guild]
 * @returns {Promise<boolean>}
 */
export const hasConsent = async function (top_user, sub_user, guild) {
  if (!(top_user instanceof Discord.User)) {
    throw new Error('top_user must be a Discord.js user object');
  }
  if (!(sub_user instanceof Discord.User)) {
    throw new Error('sub_user must be a Discord.js user object');
  }

  if (guild && !(await isUserInGuild(guild, top_user.id))) {
    return false;
  }

  const consent_list = await getConsentList(sub_user.id, top_user.id);
  return Promise.resolve(testConsent(consent_list, top_user.id));
}

/**
 * Same as hasConsent() except it takes IDs instead of objects
 * @param {int} top_user_id
 * @param {int} sub_user_id
 * @param {Discord.Guild} [guild]
 * @returns {Promise<boolean>}
 */
export const hasConsentByID = async function (top_user_id, sub_user_id, guild) {
  if (guild && !(await isUserInGuild(guild, top_user_id))) {
    return false;
  }

  const consent_list = await getConsentList(sub_user_id, top_user_id);
  return Promise.resolve(testConsent(consent_list, top_user_id));
}

function testConsent(consent_list, top_user_id) {
  if (consent_list.safeword) {
    return false;
  }
  if (consent_list.everyone) {
    return true;
  }
  if (consent_list[top_user_id]) {
    return true;
  }
  return false;
}

async function getConsentList(sub_user_id) {
  const consent_list = ensureBrainKeyExists('consent');
  if (consent_list[sub_user_id]) {
    return consent_list[sub_user_id];
  }
  return false;
}

export async function getAndSortReceivedConsent(sender, guild) {
  const consent_list = ensureBrainKeyExists('consent');

  let this_user_tops_to = [];
  for (let sub_id in consent_list) {
    for (let top_id in consent_list[sub_id]) {
      if (top_id === sender.id || top_id === 'everyone') {
        try {
          let sub = await getGuildMemberByUserID(guild, sub_id);
          this_user_tops_to.push(`${sub.displayName}` + (top_id === 'everyone' ? ' (everyone)' : ''));
        } catch { }
      }
    }
  }

  if (this_user_tops_to.length > 0) {
    this_user_tops_to.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
    let list = this_user_tops_to.map(item => `• ${item}`).join('\n');
    return { hasConsent: true, list };
  } else {
    return { hasConsent: false };
  }
}

export async function getAndSortGivenConsent(sender, guild) {
  const consented = await getConsentList(sender.id);

  let this_user_gives_to = [];
  for (let id in consented) {
    try {
      let top = await getGuildMemberByUserID(guild, id);
      this_user_gives_to.push(`${top.displayName}`);
    } catch { }
  }

  if (this_user_gives_to.length > 0) {
    this_user_gives_to.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
    let list = this_user_gives_to.map(item => `• ${item}`).join('\n');
    return { hasGivenConsent: true, list };
  } else {
    return { hasGivenConsent: false };
  }
}
export const giveConsent = async function (consentingUser, receivingUser) {
  const consent_list = ensureBrainKeyExists('consent', consentingUser.id);

  if (consent_list[receivingUser.id]) {
    return false; // consent already given
  }

  consent_list[receivingUser.id] = {
    'id': receivingUser.id,
    'time': new Date(),
  }
  await brain.write();
  return true; // consent newly given
}

export const giveConsentToAll = async function (consentingUser) {
  const consent_list = ensureBrainKeyExists('consent', consentingUser.id);

  if (consent_list.everyone) {
    return false; // consent to all already given
  }

  consent_list.everyone = {
    'time': new Date(),
  }
  await brain.write();
  return true; // consent to all newly given
}

export const revokeConsent = async function (consentingUser, receivingUser) {
  let consent_list = await getConsentList(consentingUser.id);
  if (consent_list[receivingUser.id]) {
    delete consent_list[receivingUser.id];
    await brain.write();
    return true;
  } else {
    return false;
  }
}

export const revokeConsentFromAll = async function (consentingUser) {
  let consent_list = await getConsentList(consentingUser.id);
  if (consent_list.everyone) {
    delete consent_list.everyone;
    await brain.write();
    return true;
  } else {
    return false;
  }
}

export const useSafeword = async function (consentingUser) {
  let consent_list = await getConsentList(consentingUser.id);
  if (!consent_list.safeword) {
    // if no safeword set, add one
    consent_list.safeword = true;
    await brain.write();
    return true;
  } else {
    return false;
  }
}

export const endSafeword = async function (consentingUser) {
  let consent_list = await getConsentList(consentingUser.id);
  if (consent_list.safeword) {
    delete consent_list.safeword;
    await brain.write();
    return true;
  } else {
    return false;
  }
}

export const isSafewordActive = async function (consentingUser) {
  let consent_list = await getConsentList(consentingUser.id);
  return !!consent_list.safeword;
}

export const getTopConsentedUsers = async function(guild) {
  let consentData = brain.data.consent;
  let userConsents = {};

  // Iterate over the consent data to count the number of consents each user has received
  for (let giverId in consentData) {
    for (let receiverId in consentData[giverId]) {
      if (receiverId !== 'everyone' && receiverId !== 'safeword' && await isUserInGuild(guild, receiverId)) {
        userConsents[receiverId] = (userConsents[receiverId] || 0) + 1;
      }
    }
  }

  // Convert the userConsents object to an array and sort it by the number of consents
  let sortedUserConsents = Object.entries(userConsents).sort(([, aConsents], [, bConsents]) => bConsents - aConsents);

  // Map the array of entries to an array of user objects
  let topUsers = [];
  for (let [userId, consents] of sortedUserConsents) {
    if (await isUserInGuild(guild, userId)) { // Check if user is still in the guild
      let member = await getGuildMemberByUserID(guild, userId);
      let displayName = member ? member.user.displayName : `Unknown User (${userId})`;
      topUsers.push({username: displayName, consents_received: consents});
    }
  }

  // Limit the result to the top 10 users
  return topUsers.slice(0, 10);
}

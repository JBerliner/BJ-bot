import { promises as fs } from 'node:fs';


async function importModule(url) {
  return import(url);
}

/** Loads all modules in a directory of the source code.
 * @param {string} dirName the name of a subdir of src, e.g. 'commands'
 * @param {boolean} recursive whether to include descendant directories
 * @returns {Generator<Promise<object>>} a generator over the loaded modules
 */
export async function * importModulesInDir(dirName, { recursive } = {}) {
  const dirQueue = [new URL(`../${dirName}/`, import.meta.url)];
  while (dirQueue.length > 0) {
    const dir = dirQueue.pop();
    const entries = await fs.readdir(dir, { withFileTypes: true });

    const index = entries.find(entry =>
      entry.isFile()
      && /^index\.([mc]?js)$/.test(entry.name)
    );

    if (index) {
      yield importModule(new URL(index.name, dir));
    } else {
      for (const entry of entries) {
        if (entry.isFile() && /\.([mc]?js)$/.test(entry.name)) {
          yield importModule(new URL(entry.name, dir))
        } else if (recursive && entry.isDirectory()) {
          dirQueue.push(new URL(entry.name + '/', dir));
        }
      }
    }
  }
}

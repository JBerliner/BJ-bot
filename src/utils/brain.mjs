import { Low } from 'lowdb';
import { JSONFile } from 'lowdb/node'
import { initBadJanetBrain } from './badJanetBrain.mjs';

import { config } from 'dotenv';

config(); // Load environment variables from .env file

const adapter = new JSONFile(process.env.BRAIN_FILE);
const brain = new Low(adapter);

// Initialize the database
async function initBrain() {
  await brain.read(); // Load the database from file

  // deal with the brainless condition
  if (brain.data == null) {
    brain.data = {};
  }


  // fill from bad janet
  // const badJanetBrain = await initBadJanetBrain();
  // if (!brain.data.consent) {
  //   brain.data.consent = badJanetBrain.data.consent;
  // }

  // Write any changes to the database back to the file
  await brain.write();

  return brain.data;
}

// This was rewritten by ChatGPT and I have no idea how it actually works
// It will navigate through the brain.data object accordingly,
// creating new objects at each level if they do not exist.
function ensureBrainKeyExists(...keys) {
  // Recursive helper function
  function navigateKeys(data, ...remainingKeys) {
    if (remainingKeys.length === 0) {
      return data;
    }

    const [firstKey, ...otherKeys] = remainingKeys;

    if (!data[firstKey]) {
      data[firstKey] = {};
    }

    return navigateKeys(data[firstKey], ...otherKeys);
  }

  return navigateKeys(brain.data, ...keys);
}

/**
 * Same as ensureBrainKeyExists, except returns a promise
 * because sometimes you need that, but sometimes you don't
 * so this gives you a choice
 */
function ensureBrainKeyExistsPromise(...keys) {
  return new Promise((resolve, reject) => {
    try {
      const result = ensureBrainKeyExists(...keys);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
}



// Export the 'initBrain' function and the 'brain' object for use in other files
export { initBrain, brain, ensureBrainKeyExists, ensureBrainKeyExistsPromise };

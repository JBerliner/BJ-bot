// Description:
//   Clears out any set shame roles in case the timer doesn't do it.
//
// Dependencies:
//   node-cron

import { brain, ensureBrainKeyExists } from '../utils/brain.mjs';
import {clearShameUserRecord} from '../utils/shame.mjs';

const shame_role = process.env.SHAME_ROLE;
const logs_room = process.env.LOGS_ROOM;

export default {
  // Cron schedule string - runs every minute at 1 second after each of these times
  schedule: '1 0,4,5,15,30,45 * * * *',

  // Cron task function - removes shame role for users whose timer has expired
  task: async function(client) {
    // Check for users with expired shame timers
    let shame_list = ensureBrainKeyExists('count_fail_shame_list');

    // Loop through each user id in the shame list
    for (let user_id in shame_list) {
      let row = shame_list[user_id];
      let over_time = new Date(row.over);

      if (over_time <= new Date()) {
        const log_channel = client.channels.cache.get(logs_room);

        // Get user by ID
        let user;
        try {
          user = await client.guilds.cache.get(process.env.GUILD_ID).members.fetch(user_id);
        } catch (error) {
          console.error(`Failed to fetch member ${user_id}`, error);
        }

        if (!user) {
          // User not found, delete brain key and continue
          log_channel.send(`Failed to fetch member <@${user_id}> for shame. Removing log.`);
          await clearShameUserRecord(user_id);
          continue;
        }

        // Remove the shame role from the user
        let role = user.guild.roles.cache.find(role => role.name === shame_role);
        if (user.roles.cache.some(role => role.name === shame_role)) {
          // Only try to remove the role if the user has it
          await user.roles.remove(role);
          log_channel.send(`Shame expired for <@${user.id}>`);
        } else {
          log_channel.send(`Shame key removed by cron timer for <@${user.id}>`);
        }
        await clearShameUserRecord(user_id);

        // if someone has an open counting punishment, clear it
        if (brain.data.shame_counts[user_id]) {
          delete brain.data.shame_counts[user_id];
          await brain.write();
        }
      }
    }
  }
}

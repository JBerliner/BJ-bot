// Description:
//    Updates your corner wait time to 2 hours from time of command
//
// Commands:
//    ?waitcorner

import { shameUpdate, userHasShameByID } from '../utils/shame.mjs';

/**
 * ?waitcorner
 */
export const waitcorner = {
  regex: /^\?waitcorner/,
  tag_bot: false,
  execute: async function(message) {
    let sender = message.author; // User object
    userHasShameByID(message.guild, message.author)
      .then(async flag => {
        if (!flag) {
          message.reply('What? You don\'t have that role.');
        } else {
          await shameUpdate(sender.id, 7200000);
          message.delete();
          message.channel.send(`<@${sender.id}> has chosen to wait for their corner of shame to end.`);
        }
      })
  }
}

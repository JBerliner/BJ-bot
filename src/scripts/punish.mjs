import {brain, ensureBrainKeyExists, ensureBrainKeyExistsPromise} from '../utils/brain.mjs';
import {
  getShameUserRecord,
  createShameRecord,
  defaultCount,
  setupShameCount,
  doesShameCountExist,
  addToTarget,
  clearShameUserRecord, calculatePunishment, addShameByUserId, shameUpdate,
} from '../utils/shame.mjs';
import {Slowdown} from '../utils/classes/Slowdown.mjs';
import {getGuildMemberByUserID} from '../utils/users.mjs';
import _ from 'lodash';

const shame_room = process.env.SHAME_ROOM;
const shame_role = process.env.SHAME_ROLE;
const logs_room = process.env.LOGS_ROOM;


export const punish_and_count = {
  regex: /^\?punish$/i,
  tag_bot: false,
  execute: async function(message) {
    const user = message.author;
    if (shame_room === message.channel.id) {
      // Check if the user is already being punished
      if (doesShameCountExist(user) && brain.data.shame_counts[user.id].stopped !== true) {
        message.reply(`You're already being punished! Keep counting to ${brain.data.shame_counts[user.id].target}.`);
        return;
      }

      let count_to = defaultCount;
      let reply_text = 'You\'ve acted shamefully! Now you are in the Corner of Shame.\n';

      // get any existing shame record
      const shame = await getShameUserRecord(user);
      shame.stopped = false;
      await brain.write();

      // if there's a valid shame.count_to, we use it as count_to
      if (shame && shame.count_to > 0) {
        count_to = shame.count_to;
      }

      // look for having messed up the count.
      let number = 0;
      let using_last_fail = false;
      await ensureBrainKeyExistsPromise('count_fail_tracking_number');
      if (brain.data.count_fail_tracking_number[user.id]?.last_fail_number) {
        number = brain.data.count_fail_tracking_number[user.id].last_fail_number;
        const new_count_to = calculatePunishment(number);
        if (new_count_to > count_to) {
          reply_text += 'Oh *snap,* you messed up counting at ' + number + '!!\n';
          count_to = new_count_to;
          using_last_fail = true;
        }
      }

      // Setup shame_counts after we have the final count_to
      await setupShameCount(user, count_to, using_last_fail);
      brain.data.shame_counts[user.id].stopped = false;
      await brain.write();

      message.reply(reply_text + `Count up to **${count_to}** to get out!`);

      // Slowdown part
      // this code is much simpler using a separate function even if it's only ever used in one place.
      let {
        slowdown,
        onetime,
        countMoreThanOne,
        selfImposed,
        modifier,
        top_id,
        count
      } = await calculateSlowdown(user, shame?.one_time_slow);

      let slowdownMessage;
      if (slowdown) {
        slowdownMessage = `You've been slowed down! The room is now set to a ${slowdown} second slow mode.\n`;
        if (countMoreThanOne) {
          slowdownMessage += `You had ${count} slowdowns. The highest was chosen.`;
        }
        if (modifier > 0) {
          slowdownMessage += `\n**Oh snap** you messed up the counting! **${modifier} seconds** of your slowdown came from messing up the count. (Use \`slow modifier?\` for more information.)`;
        }
        if (selfImposed) {
          slowdownMessage += '\nYou have only yourself to blame.';
        } else if (top_id) {
          slowdownMessage += `\nYou should thank <@${top_id}> for that.`;
        }
      } else {
        slowdownMessage = 'Congrats, you don\'t have any slowdown set. Still, don\'t go too fast or I might miss some numbers.\n';
      }
      if (onetime || onetime === 0) {
        slowdownMessage += 'This is a one-time slowdown.';
      }
      const member = message.member;
      await message.channel.setRateLimitPerUser(slowdown, `Slow down for ${member.displayName} based on their settings`);
      message.channel.send(slowdownMessage);
      await brain.write();
    } else {
      // User attempted to trigger punishment outside of the shame room
      if (doesShameCountExist(user)) {
        await addToTarget(user, 100);

        // update shame
        const shame = await getShameUserRecord(user);
        if (!shame) {
          await createShameRecord(user, 0, null, 'punish_elsewhere', brain.data.shame_counts[user.id].target);
        } else {
          shame.count_to = brain.data.shame_counts[user.id].target;
          await brain.write();
        }
        await addShameByUserId(message.guild, user.id);
        message.reply('Shame on you for even *trying* that in another channel. ' +
          `Your target is now ${brain.data.shame_counts[user.id].target}. ` +
          `Now get over to <#${shame_room}> and count your way out.`);
      } else {
        await createShameRecord(user, 0, null, 'punish_elsewhere', 150);
        await addShameByUserId(message.guild, user.id);
        message.reply('Shame on you for even *trying* that in another channel!! You can count your way out to 150!' +
          ` Now get over to <#${shame_room}> and count your way out.`);
      }
    }
  }
};

export async function calculateSlowdown(user, overrideSlowdown = null) {
  if (overrideSlowdown !== null) {
    // Override provided, skip calculations and return override values
    return {
      slowdown: overrideSlowdown,
      onetime: true,
      countMoreThanOne: false,
      selfImposed: false,
      modifier: 0,
      top_id: null, // If the slowdown is manually set, there is no 'top_id'
      count: 0
    };
  }

  const slowdownClass = new Slowdown(user);
  let slowdown = ensureBrainKeyExists('slowdown', user.id);
  let modifier = await slowdownClass.getCountModifierSeconds();
  let s = 0, countMoreThanOne = false, selfImposed = false, top_id = null, count = 0;

  if (!(_.isEmpty(slowdown)) || modifier !== 0) {
    top_id = await slowdownClass.getSlowRecordSetterId();
    count = await slowdownClass.getSlowRecordCount();
    s = await slowdownClass.getSlowRecordSeconds();
    countMoreThanOne = count > 1;

    if (modifier > 0) {
      s = +s + +modifier;
    }

    selfImposed = top_id && slowdown[top_id]?.self;
  }

  return {
    slowdown: s,
    onetime: false,
    countMoreThanOne,
    selfImposed,
    modifier,
    top_id,
    count
  };
}


export const increment = {
  regex: /^(\d+)$/,
  tag_bot: false,
  execute: async function(message) {
    if (shame_room === message.channel.id) {
      const user = message.author;
      // Check if the user is currently being punished
      if (!(brain.data.shame_counts && brain.data.shame_counts[user.id])) {
        message.reply('Did you read the directions?');
        return;
      }

      // reset shame expiration
      const shame_data = ensureBrainKeyExists('count_fail_shame_list', user.id);
      const two_hours = 60 * 60 * 2 * 1000;
      if (shame_data.timer_ms < two_hours) {
        await shameUpdate(user.id, two_hours);
      }

      const userCount = brain.data.shame_counts[user.id].count;
      const targetCount = brain.data.shame_counts[user.id].target;

      if (parseInt(message.content) === userCount + 1) {
        brain.data.shame_counts[user.id].count++;
        await brain.write();
        message.react('✅');

        if (brain.data.shame_counts[user.id].count >= targetCount) {
          // handle the last fail
          if (brain.data.shame_counts[user.id]?.using_last_fail) {
            // idk just make sure it exists before deleting
            await ensureBrainKeyExistsPromise('count_fail_tracking_number', user.id, 'last_fail_number');
            delete brain.data.count_fail_tracking_number[user.id];
          }
          // Delete this AFTER accessing it for the last time
          delete brain.data.shame_counts[user.id];
          await brain.write();

          await clearShameUserRecord(user.id);
          let guildMember = await getGuildMemberByUserID(message.guild, user.id);

          let role = guildMember.guild.roles.cache.find(role => role.name === shame_role);
          if (guildMember.roles.cache.some(role => role.name === shame_role)) {
            // Only try to remove the role if the user has it
            await guildMember.roles.remove(role);
            const log_channel = message.client.channels.cache.get(logs_room);
            await log_channel.send(`Shame counted away by <@${guildMember.id}>`);
          }
          message.channel.send("You made it! You're released from the Corner of Shame! " +
            "Let's not repeat the same mistakes, shall we?");

          // Delete all messages that are not pinned
          await delay(10000); // 10 second delay
          await deleteAllUnpinnedMessages(message.channel);
        }
      } else {
        message.react('❌');
        // Get the user's current shame record
        const shame = await getShameUserRecord(user);
        // If there's a valid mistake penalty, apply it
        if (shame && shame.mistake_penalty) {
          await addToTarget(user, shame.mistake_penalty);
          message.reply(`Wrong! Your next number is ${userCount + 1}. Your target has increased to ${brain.data.shame_counts[user.id].target}.`);
        } else {
          message.reply(`Wrong! Your next number is ${userCount + 1}`);
        }
      }
    }
  }
};

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function deleteAllUnpinnedMessages(channel) {
  let messagesToDelete;
  do {
    // Fetch the messages
    const fetchedMessages = await channel.messages.fetch({ limit: 100 });
    // Filter out the pinned messages and messages that are more than 14 days old
    messagesToDelete = fetchedMessages.filter(
      (msg) => !msg.pinned && Date.now() - msg.createdTimestamp < 14 * 24 * 60 * 60 * 1000
    );
    // Bulk delete the filtered messages
    await channel.bulkDelete(messagesToDelete);
    await delay(2000); // 2 second delay
  } while (messagesToDelete.size !== 0); // Repeat until there are no more messages to delete

  // Delete individual messages older than 14 days
  await delay(2000); // 2 second delay
  const fetchedMessages = await channel.messages.fetch({ limit: 100 });
  const oldUnpinnedMessages = fetchedMessages.filter((msg) => !msg.pinned && Date.now() - msg.createdTimestamp >= 14 * 24 * 60 * 60 * 1000);
  for (const msg of oldUnpinnedMessages.values()) {
    await msg.delete();
    await delay(1000); // 1 second delay
  }
}

export const other_talk = {
  regex: /.*$/,
  tag_bot: false,
  execute: async function (message) {
    if (shame_room === message.channel.id) {
      // Ignore list
      const ignoreList = [/^\d+$/, /^\?punish$/i, /^stoppunish$/i];

      // Check if the message matches any pattern in the ignore list
      const isIgnored = ignoreList.some(ignorePattern => ignorePattern.test(message.content));
      if (!isIgnored && (brain.data.shame_counts && brain.data.shame_counts[message.author.id])) {
        await addShameCountingViolation(message, 'Who said you could talk!');
      }
    } else {
      // things said elsewhere
      // if user is actively in a punishment
      const user = message.author;
      if (doesShameCountExist(user) && brain.data.shame_counts[user.id].stopped !== true) {
        // user is in an active punishment
        await addShameCountingViolation(message, 'Shouldn\'t you be counting?');
      }
    }
  }
};

async function addShameCountingViolation(message, reply) {
  message.react('❌');
  const user = message.author;
  brain.data.shame_counts[user.id].violations++;
  const violations = brain.data.shame_counts[user.id].violations;
  await brain.write();
  reply += ' A violation has been recorded to this punishment.\n';
  reply += `You currently have ${violations} violation${violations > 1 ? 's' : ''}. Penalties grow with the violation count.\n\n`
  if (violations > 1) {
    // add 5 * violation to count
    let penalty = (violations - 1) * 5;
    const target = await addToTarget(user, penalty);
    reply += `**A penalty of ${penalty} has been added to your count. You now need to count to ${target}**`;
  } else {
    reply += 'The first violation was free. You need to work off your shame!\n\n' +
      'Further violations will result in an increased count. If you need help run ' +
      '`stoppunish` in <#928783575627751505> to stop counting out and be free to talk again. ' +
      'But that will start you over when you\'re ready to go again.\n\n' +
      'Running `stoppunish` will *not* reset your violations for this session.';
  }
  message.reply(reply);
}

export const stoppunish = {
  regex: /^stoppunish$/i,
  tag_bot: false,
  execute: async function(message) {
    if (shame_room === message.channel.id) {
      const user = message.author;
      // Only the punished user can stop the punishment
      if (brain.data.shame_counts && brain.data.shame_counts.hasOwnProperty(user.id)) {
        brain.data.shame_counts[user.id].count = 0;
        brain.data.shame_counts[user.id].stopped = true;
        await brain.write();
        message.reply('Stopping. Make sure to try again.');
      } else {
        message.reply("You're not currently counting in the Corner of Shame!");
      }
    }
  }
};

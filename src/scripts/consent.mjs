import {
  endSafeword,
  getAndSortGivenConsent,
  getAndSortReceivedConsent, getTopConsentedUsers,
  giveConsent, giveConsentToAll,
  revokeConsent, revokeConsentFromAll,
  useSafeword,
} from '../utils/consent.mjs';
import {getGuildMemberByUserID} from '../utils/users.mjs';

export const consent_info = {
  regex: /^consent\?$/i,
  tag_bot: true,
  execute: function(message) {
    message.reply(`**Consent to Mess With**
:small_orange_diamond:\`@BJ consent @user\` or \`@BJ consent to @user\` - Consent to let the mentioned user mess with you using bot commands

:small_orange_diamond:\`@BJ end consent @user\` or \`@BJ end consent to @user\` - End consent to let the mentioned user mess with you using bot commands

:small_orange_diamond:\`@BJ consent all\` or \`@BJ consent to all\` - Consent to let all users mess with you using bot commands

:small_orange_diamond:\`@BJ end consent all\` or \`@BJ end consent to all\` - End consent to let all users mess with you using bot commands

:small_orange_diamond:\`@BJ safeword\` or \`@BJ red\` or \`@BJ pineapple\` - Pause all consent. No one can mess with you right now.

:small_orange_diamond:\`@BJ end safeword\` - Unpause all consent. Anyone you've authorized can mess with you again.

**Other Commands**

:diamond_shape_with_a_dot_inside:\`@BJ given consent\`- See all you've given consent to (does *not* tag/ping users)

:diamond_shape_with_a_dot_inside:\`@BJ received consent\`- See all you've received consent from (does *not* tag/ping users)

:diamond_shape_with_a_dot_inside:\`@BJ top consented\` list users with the most consent received (does *not* tag/ping users)`
    );
  }
}

export const end_consent_no_target = {
  regex: /^end consent$/i,
  tag_bot: true,
  execute: function(message) {
    message.reply('Command not correctly formed. Did you forget to tag who you want to end consent to?');
  }
}

export const consent_no_target = {
  regex: /^consent$/i,
  tag_bot: true,
  execute: function(message) {
    message.reply('Command not correctly formed. Did you forget to tag who you want to consent to?');
  }
}

export const consent_to = {
  regex: /^consent (to )?/i,
  tag_bot: true,
  execute: async function(message) {
    let sender = message.author;
    let mentioned_user = message.mentions.users.first(2)[1];

    if (mentioned_user) {
      await giveConsent(sender, mentioned_user);
      const member = await getGuildMemberByUserID(message.guild, mentioned_user.id);
      message.reply(`You've given consent to: ${member.displayName}`);
    } else {
      let isToAll = message.content.match(/consent (to )?all/i);
      if (isToAll) {
        await giveConsentToAll(sender);
        message.reply('Everyone has been given consent to mess with you using my commands.');
      } else {
        message.reply('Command not correctly formed. The command is: '
          + '\n> <@1078719671022395442> consent to @usertag\n or'
          + '\n> <@1078719671022395442> consent to all'
          + '\n\n'
          + 'Were you trying to use '
          + '\n> <@1078719671022395442> received consent\n or'
          + '\n> <@1078719671022395442> given consent\n'
        );
      }
    }
  }
}
export const end_consent = {
  regex: /^end consent (to )?/i,
  tag_bot: true,
  execute: async function(message) {
    let sender = message.author;
    let mentioned_user = message.mentions.users.first(2)[1];

    if (mentioned_user) {
      let revokeStatus = await revokeConsent(sender, mentioned_user);
      if (revokeStatus) {
        const member = await getGuildMemberByUserID(message.guild, mentioned_user.id);
        message.reply(`You've revoked consent from: ${member.displayName}`);
      } else {
        message.reply('I wasn\'t able to find any consent on record to that user. ' +
          'If this is a mistake, please let beta know.\n' +
          'Please note that you cannot revoke consent to someone you did not explicitly ' +
          'give consent to ("all" doesn\'t count).');
      }
    } else {
      let isToAll = message.content.match(/end consent (to )?all/i);
      if (isToAll) {
        let revokeAllStatus = await revokeConsentFromAll(sender);
        if (revokeAllStatus) {
          message.reply('Consent to everyone has been revoked.');
        } else {
          message.reply('I couldn\'t find consent to everyone, ' +
            'so I haven\'t changed anything. If this is a mistake, please let beta know.');
        }
      } else {
        message.reply('Command not correctly formed. The command is \n' +
          '> <@1078719671022395442> end consent to @usertag or \n' +
          '> <@1078719671022395442> end consent to all');
      }
    }
  }
}

export const safeword = {
  regex: /^(safeword|red|pineapple)/i,
  tag_bot: true,
  execute: async function(message) {
    let sender = message.author;

    if (!await useSafeword(sender)) {
      message.reply('Your safeword was already on.');
    } else {
      message.reply('Okay. No one can mess with you right now. Take a break. ' +
        'If/when you want to end your safeword, send \n> <@1078719671022395442> end safeword');
    }
  }
}

export const end_safeword = {
  regex: /^end safeword/i,
  tag_bot: true,
  execute: async function(message) {
    let sender = message.author;

    if (!await endSafeword(sender)) {
      message.reply('Your safeword wasn\'t set, so nothing has changed.');
    } else {
      message.reply('Okay. Your consent settings are still as you set them but ' +
        'people are allowed to mess with you again if you\'ve given them consent.');
    }
  }
}

export const received_consent = {
  regex: /^received consent/i,
  tag_bot: true,
  execute: async function(message) {
    message.reply('Did you know this is a slash command now? ' +
      'Check out `/consent_received`\n' +
      'Give me a moment to organize the names...');
    let sender = message.author;

    const result = await getAndSortReceivedConsent(sender, message.guild);

    if (result.hasConsent) {
      message.reply(`You've received consent from:\n${result.list}`);
    } else {
      message.reply('You haven\'t received consent from anyone yet.');
    }
  }
}

export const given_consent = {
  regex: /^given consent/i,
  tag_bot: true,
  execute: async function(message) {
    message.reply('Did you know this is a slash command now? ' +
      'Check out `/consent_given`\n' +
      'Give me a moment to organize the names...');
    let sender = message.author;

    const result = await getAndSortGivenConsent(sender, message.guild);

    if (result.hasGivenConsent) {
      message.reply(`You've given consent to:\n${result.list}`);
    } else {
      message.reply('You haven\'t given consent to anyone yet.');
    }
  }
}

export const top_consented = {
  regex: /^top consented$/i,
  tag_bot: true,
  execute: async function(message) {
    let topUsers = await getTopConsentedUsers(message.guild);
    let reply = '**Top users with most consent received**\n';

    topUsers.forEach((user, index) => {
      reply += `${index + 1}. ${user.username} - ${user.consents_received}\n`;
    });

    message.reply(reply);
  }
}

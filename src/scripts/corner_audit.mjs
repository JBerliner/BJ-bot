// Description:
// audit what's in the brain for the shame role
//
// Commands:
//   corner audit

import { ensureBrainKeyExists } from '../utils/brain.mjs';
import { getGuildMemberByUserID } from '../utils/users.mjs';
import { MessageStacker } from '../utils/classes/MessageStacker.mjs';

const admin_room = process.env.BOT_ADMIN_ROOM;

/**
 * corner audit
 */
export const corneraudit = {
  regex: /^corner audit/i,
  tag_bot: false,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.channel.send("You can't to that here.");
    } else {
      let list = ensureBrainKeyExists('count_fail_shame_list');
      const stacker = new MessageStacker(message, '');

      for (let user_id in list) {
        let reply = '';
        try {
          let user = await getGuildMemberByUserID(message.guild, user_id);
          reply += `**${user.displayName}**\n`;
        } catch (error) {
          reply += `**<@${user_id}>** (Unknown user)\n`;
        }
        let received = new Date(list[user_id].received);
        reply += `Received: ${received}\n`;
        reply += `Timer (ms): ${list[user_id].timer_ms}\n`;
        let over = new Date(list[user_id].over);
        reply += `Over: ${over}\n`;
        reply += `Source: ${list[user_id].source} \n`;
        reply += `Number: ${list[user_id].number}\n`;
        await stacker.appendOrSend(reply);
      }
      await stacker.finalize('', 'The corner is empty.');
    }
  }
}

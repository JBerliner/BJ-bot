// Description:
// ground a user and see their grounding status

import { brain } from '../utils/brain.mjs';
import { groundingMessages } from '../utils/grounding.mjs';
import { getFirstUserIdFromMentionOnRespond } from '../utils/users.mjs';
const admin_room = process.env.BOT_ADMIN_ROOM;


/**
 *
 * @bot ground <@user> {minutes}m min{session_minutes} {precent}%
 */
export const ground = {
  regex: /^ground <@\d+>/i,
  tag_bot: true,
  execute: async function(message) {
    message.reply('Due to bugs in this command, ' +
      'and for ease of development, ' +
      'it has been moved entirely to the slash command. \n' +
      'Please use `/ground_user` to ground a user.\n\n' +
      'Sorry 😔');
  },
};



/**
 * check on {@user} grounding
 */
export const checkGroundingCommand = {
  regex: /^check\s?(on)?\s?<@\d+>\s*grounding/i,
  tag_bot: true,
  execute: async function(message) {
    let user = getFirstUserIdFromMentionOnRespond(message);
    if (user) {
      let reply = groundingMessages.statusMessage(user);
      message.reply(reply);
    } else {
      message.reply('Sorry, I could not find the specified user.');
    }
  }
};


/**
   * This code allows the admin to view a list of all users with groundings.
   *
   * grounding audit
   */
export const groundAudit = {
  regex: /^grounding audit/i,
  tag_bot: false,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel) {
      message.reply("You can't to that here.");
      return;
    }
    let grounding = brain.data.grounding;
    let users = Object.keys(grounding);
    let reply = '';
    for (let i = 0; i < users.length; i++) {
      let user_id = users[i];
      reply += groundingMessages.statusMessage(user_id);
    }
    if (reply) {
      message.reply(reply);
    } else {
      message.reply('No one is currently grounded.');
    }
  }
};

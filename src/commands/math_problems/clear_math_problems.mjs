import { PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { brain, ensureBrainKeyExists } from '../../utils/brain.mjs';
import { problemUtils } from '../../utils/math_problems.mjs';

export const data = new SlashCommandBuilder()
  .setName('clear_math_problems')
  .setDescription('Manually clear a users math problems, will also delete their channel if they have one.')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you wish to clear.')
    .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  ensureBrainKeyExists('problems', user.id);
  if (brain.data.problems[user.id]) {
    let savedChannelId = brain.data.problems[user.id].channel;
    await problemUtils.clearProblems(user.id);
    if (interaction.guild.channels.cache.get(savedChannelId) !== undefined) {
      interaction.guild.channels.cache.get(savedChannelId).delete();
    }
    await interaction.reply({content: 'Math problems cleared successfully', ephemeral: true});
  } else {
    await interaction.reply({content: 'That user has no math problems assigned.', ephemeral: true});
  }
}

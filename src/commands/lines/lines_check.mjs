import { SlashCommandBuilder } from 'discord.js';
import {brain, ensureBrainKeyExists} from '../../utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('lines_check')
  .setDescription('Check a user\'s lines status')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to check the lines status of')
    .setRequired(true));

export async function execute(interaction) {
  const user = interaction.options.getUser('user');

  ensureBrainKeyExists('lines');
  const record = brain.data.lines[user.id];
  if (typeof record === "undefined") {
    await interaction.reply(`<@${user.id}> currently has no lines to write.`);
  } else {
    let lines_todo = `<@${user.id}> has ${record.lines_todo} lines to write, `;

    let completed = record.lines_completed ?
      `${record.lines_completed} lines have been completed.\n` :
      `Nothing has been completed yet so there's still ${record.lines_todo} lines to do.`;

    const left = record.lines_todo - record.lines_completed;
    let left_msg = left === record.lines_todo ? '' : `${left} lines left.`;

    await interaction.reply(lines_todo + completed + left_msg);
  }
}

import { PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { brain, ensureBrainKeyExists } from '../../utils/brain.mjs';
import {lineUtils} from '../../utils/lines.mjs';

export const data = new SlashCommandBuilder()
  .setName('clear_lines')
  .setDescription('Manually clear a users lines, will also delete their channel if they have one.')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you wish to clear.')
    .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  ensureBrainKeyExists('lines', user.id);
  if (brain.data.lines[user.id]) {
    let savedChannelId = brain.data.lines[user.id].channel;
    await lineUtils.clearLines(user.id);
    if (interaction.guild.channels.cache.get(savedChannelId) !== undefined) {
      interaction.guild.channels.cache.get(savedChannelId).delete();
    }
    await interaction.reply({content: 'Lines cleared successfully', ephemeral: true});
  } else {
    await interaction.reply({content: 'That user has no lines assigned.', ephemeral: true});
  }
}

import { SlashCommandBuilder } from 'discord.js';
import {lineUtils} from '../../utils/lines.mjs';
import { hasConsent } from '../../utils/consent.mjs';
import {backfires} from '../../utils/backfire.mjs';
import {pingsEveryone} from '../../utils/security.mjs';
import {addShameByUserId, createShameRecord} from '../../utils/shame.mjs';

export const data = new SlashCommandBuilder()
  .setName('assign_lines')
  .setDescription('Assigns a line to a user')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to assign lines to.')
    .setRequired(true))
  .addIntegerOption(option => option.setName('num_lines')
    .setDescription('The number of lines the user should type.')
    .setRequired(true))
  .addStringOption(option => option.setName('line')
    .setDescription('The line you want the user to write. If left empty, random lines will be generated.')
    .setRequired(false))
  .addStringOption(option => option.setName('personalized_instructions')
    .setDescription('Special instructions for the lines (free text) - please use ||spoiler text|| for anything NSFW.')
    .setRequired(false))
  .addBooleanOption(option => option.setName('hide_number_completed')
    .setDescription('Set whether the number of lines completed should be hidden')
    .setRequired(false))
  .addStringOption(option => option.setName('random_case')
    .setDescription('Set whether the typist should have to type the lines WiTH rAndOME CapiTaLIZaTIon')
    .setRequired(false)
    // When I first implemented random case I wrote a bug that made it so that
    // when assigning random lines, random_case only affects the line after a
    // mistake. Buggy mode reenables this bug. If lines aren't random, it will
    // also only randomize on errors.
    //
    // The way this works is simple(-ish), but quite confusing.  There are two
    // if statements that branch on this value. The one that determines if the
    // line after a correctly typed line is not taken with the value "buggy",
    // but the one after an incorectly typed line is.
    .addChoices(
      {name: 'rANdoM CAsE', value: 'random'},
      {name: 'Normal case', value: 'normal'},
      {name: 'Buggy', value: 'buggy'},
    ))
  .addStringOption(option => option.setName('spectator')
    .setDescription('Controls who can view and speak in the channel.')
    .setRequired(false)
    .addChoices(
      { name: 'Off - Just you and the person doing lines', value: 'off' },
      { name: 'Viewing - everyone can see the channel', value: 'viewing' },
      { name: 'Free Talking - everyone can talk in the channel', value: 'free-talking' }
    ))
  .addIntegerOption(option => option.setName('penalty_lines')
    .setDescription('The number of extra lines the user should type for making a mistake.')
    .setRequired(false))

export async function execute(interaction) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const lines_todo = interaction.options.getInteger('num_lines');
  const line = interaction.options.getString('line');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  const command_channel = interaction.channel.id;
  const hide_number_completed = interaction.options.getBoolean('hide_number_completed');
  const random_case = interaction.options.getString('random_case');
  const penalty_lines = interaction.options.getInteger('penalty_lines');
  const spectator = interaction.options.getString('spectator') || 'off';
  let should_hide_completed = false;

  if (hide_number_completed) should_hide_completed = true;

  // check to see if user is already assigned lines
  if (lineUtils.isUserAssignedLines(interaction.member.id)) {
    await lineUtils.addLinesToDo(interaction.member.id, lines_todo);
    let reply = `***<@${sender.id}> has given <@${to_user.id}> lines to write!***\n` +
      'Hey! Wait a minute!!! Shouldn\'t you be doing your *own* lines???\n';
    if (to_user.id !== sender.id) {
      reply += `**Never mind** <@${to_user.id}>, I'm just going to add ${lines_todo} to <@${sender.id}>'s lines.\n\n`;
    } else {
      reply += `${lines_todo} have been added to your lines to do!\n\n`;
    }
      reply += 'Stop screwing around and **get to it!!**';
    await interaction.reply(reply);
    return;
  }

  if (line !== null && (
    line.startsWith('/') || // line is a slash command
    line.includes('<@') || // tags another user
    line.toLowerCase() === 'cancel' ||
    lineUtils.checkForCheating(line))
  ) {
    await interaction.reply({
      content: 'You cannot assign someone that line. Try another one.',
      ephemeral: true
    });
    return;
  }

  if (pingsEveryone(line) || pingsEveryone(personalized_instructions)) {
    await interaction.reply('Fuck you. And SHAME on you for trying to ping everyone!');
    await addShameByUserId(interaction.guild, sender.id);
    await createShameRecord(sender, 0, 86400000, 'mass_ping');
    return;
  }

  if (await backfires(interaction.member, to_user)) {
    await interaction.deferReply();
    let reply = '# Backfire!\n';
    reply += `***<@${sender.id}> tried to give <@${to_user.id}> lines to write but it backfired!***\n`;
    reply += await lineUtils.assignLinesToUser(sender, sender.id, interaction.guild, lines_todo, line, command_channel, should_hide_completed, random_case, penalty_lines, spectator);
    if (personalized_instructions) {
      reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
    }
    await interaction.editReply(reply);

    return;
  }

  if (!await hasConsent(sender, to_user)) {
    await interaction.reply({
      content: 'You do not have consent to set lines for that user.',
      ephemeral: true
    });
    return;
  }

  await interaction.deferReply();

  let reply = `***<@${sender.id}> has given <@${to_user.id}> lines to write!***\n`;
  reply += await lineUtils.assignLinesToUser(to_user, sender.id, interaction.guild, lines_todo, line, command_channel, should_hide_completed, random_case, penalty_lines, spectator);
  if (personalized_instructions) {
    reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
  }
  await interaction.editReply(reply);
}

import { SlashCommandBuilder } from 'discord.js';

import {brain, ensureBrainKeyExistsPromise} from '../../utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('unset_wfm')
  .setDescription('Remove your profile link');

export async function execute(interaction) {
  await ensureBrainKeyExistsPromise('wfm_profiles');
  if (brain.data.wfm_profiles[interaction.user.id]) {
    delete brain.data.wfm_profiles[interaction.user.id];
    await brain.write();
    interaction.reply('I\'ve removed your profile link.');
  } else {
    interaction.reply('You didn\'t have a profile link to unset. ' +
      'Did you need to set one? The link is an optional field, ' +
      'so it doesn\'t show up by default.\n' +
      'If you click on the +1 options or press {tab} after you\'ve typed in the command, ' +
      'you should have access to enter your link.');
  }
}

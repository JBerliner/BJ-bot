import {SlashCommandBuilder} from 'discord.js';
import {brain, ensureBrainKeyExists} from '../../utils/brain.mjs';
import {track} from '../../utils/track.mjs';

export const data = new SlashCommandBuilder()
  .setName('counting_shapes_give_up')
  .setDescription('Give up and have me tell you how many shapes were there.');

export async function execute(interaction) {
  const user = interaction.user;

  let shape_record = ensureBrainKeyExists('shape_count');
  if (!shape_record[user.id]) {
    return interaction.reply('You aren\'t counting any shapes right now. ' +
      'Use `/counting_shapes_start` to get started!'
    );
  }

  const correctCount = shape_record[user.id].count;
  const messageLink = shape_record[user.id].message_link;

  const endTime = Date.now();
  const timeTaken = (endTime - shape_record[user.id].start_time) / (1000 * 60); // in minutes
  await track.updateShapeCountTracking(user.id,
    false,
    timeTaken,
    shape_record[user.id].difficulty,
    shape_record[user.id].size,
    shape_record[user.id].movement,
    shape_record[user.id].colorTheme,
    0);

  // Delete the user's record as they gave up
  delete shape_record[user.id];

  await brain.write(); // save changes to the brain

  return interaction.reply(`You gave up! There were ${correctCount} shapes. Here's the [original image](${messageLink}).`);
}

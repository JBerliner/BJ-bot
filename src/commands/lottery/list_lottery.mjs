import { SlashCommandBuilder } from 'discord.js';
import {listLottery} from '../../utils/lottery.mjs';

export const data = new SlashCommandBuilder()
  .setName('list_lottery')
  .setDescription('Get a list of everyone in the lottery.')

export async function execute(interaction) {
  await listLottery(interaction, null)
}

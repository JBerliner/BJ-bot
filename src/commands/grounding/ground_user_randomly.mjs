import { SlashCommandBuilder } from 'discord.js';
import { hasConsent } from '../../utils/consent.mjs';
import { groundingUtils, groundingMessages } from '../../utils/grounding.mjs';
import { addRoleByName } from '../../utils/users.mjs';
import {backfires} from '../../utils/backfire.mjs';
import _ from 'lodash';

const grounded_role = process.env.GROUNDED_ROLE;

export const data = new SlashCommandBuilder()
  .setName('ground_user_randomly')
  .setDescription('Grounds someone for some amount of time')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to ground')
    .setRequired(true))
  .addIntegerOption(option => option.setName('min_minutes')
    .setDescription('The minimum amount of time in minutes (default 1).')
    .setRequired(false))
  .addIntegerOption(option => option.setName('min_minimum_session')
    .setDescription('The minimum minimum session duration in minutes (default 0).')
    .setRequired(false))
  .addNumberOption(option => option.setName('min_completion_percentage')
    .setDescription('The minimum required percentage of completion (default 0).')
    .setRequired(false))
  .addIntegerOption(option => option.setName('max_minutes')
    .setDescription('The maximum amount of time in minutes (default 60).')
    .setRequired(false))
  .addIntegerOption(option => option.setName('max_minimum_session')
    .setDescription('The maximum minimum session duration in minutes (default 12).')
    .setRequired(false))
  .addNumberOption(option => option.setName('max_completion_percentage')
    .setDescription('The maximum required percentage of completion (default 100).')
    .setRequired(false))
  .addStringOption(option => option
    .setName('secret_settings')
    .setDescription('Choose if the settings should be hidden or public (will randomize if not set).')
    .addChoices(
      { name: 'Settings Not Hidden', value: 'normal' },
      { name: 'Settings are a Secret!', value: 'secret' }
    )
    .setRequired(false)
  );

export async function execute(interaction) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');

  let min_minutes = interaction.options.getInteger('min_minutes');
  min_minutes = min_minutes > 0 ? min_minutes : 1;
  let max_minutes = interaction.options.getInteger('max_minutes');
  max_minutes = max_minutes > 0 ? max_minutes : 60;
  const minutes_todo = _.random(min_minutes, max_minutes, false);

  const instructions = interaction.options.getString('instruction_presets');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  
  let min_minimum_session = interaction.options.getInteger('min_minimum_session');
  min_minimum_session = min_minimum_session !== null && min_minimum_session <= 12 && min_minimum_session >= 0 ? min_minimum_session : 0;
  let max_minimum_session = interaction.options.getInteger('max_minimum_session')
  max_minimum_session = max_minimum_session !== null && max_minimum_session <= 12 && max_minimum_session >= 0 ? max_minimum_session : 12;
  const minutes_minimum = _.random(min_minimum_session, max_minimum_session, false);

  let min_percentage = interaction.options.getNumber('min_completion_percentage');
  min_percentage = min_percentage !== null && min_percentage >= 0 && min_percentage <= 100 ? min_percentage : 0;
  let max_percentage = interaction.options.getNumber('max_completion_percentage');
  max_percentage = max_percentage !== null && max_percentage >= 0 && max_percentage <= 100 ? max_percentage : 100;
  const percentage = _.random(min_percentage, max_percentage, false);
  
  const secretSetting = interaction.options.getString('secret_settings') || (Math.random() < 0.5 ? 'normal' : 'secret');

  // make sure minimum is 12 or less
  if (!groundingUtils.testMinutes(minutes_minimum)) {
    interaction.reply({content: groundingMessages.minMinuteMessage, ephemeral: true});
    return;
  }


  if (await backfires(interaction.member, to_user)) {
    if (secretSetting === 'secret') {
      await interaction.deferReply({ephemeral: true});
    } else {
      await interaction.deferReply({ephemeral: false});
    }

    // add grounded role
    await addRoleByName(sender.id, interaction.guild, grounded_role);

    // update the brain accordingly and reply with what's going on
    let reply = '# Backfire!\n';
    reply += `***<@${sender.id}> tried to give <@${to_user.id}> a grounding but it backfired!***\n`;
    const embed = await groundingUtils.groundUser(sender.id, minutes_todo, minutes_minimum, percentage, instructions, secretSetting);

    if (instructions) {
      embed.addFields({ name: 'Instructions', value: instructions});
    }

    if (personalized_instructions) {
      embed.addFields({ name: 'Personalized Instructions', value: personalized_instructions});
    }

    if (secretSetting === 'secret') {
      await interaction.editReply({
        content: 'Your grounding has been created.',
        ephemeral: true
      });
      await interaction.followUp({
        content: reply,
        embeds: [embed]
      });
    } else {
      await interaction.editReply({
        content: reply,
        embeds: [embed]
      });
    }
    return;
  }

  // check consent
  if (!await hasConsent(sender, to_user)) {
    interaction.reply({
      content: 'You do not have consent to ground that user.',
      ephemeral: true
    });
    return;
  }


  if (secretSetting === 'secret') {
    await interaction.deferReply({ephemeral: true});
  } else {
    await interaction.deferReply({ephemeral: false});
  }

  // add grounded role
  await addRoleByName(to_user.id, interaction.guild, grounded_role);

  // update the brain accordingly and reply with what's going on
  const reply = `***<@${sender.id}> has given <@${to_user.id}> a random grounding!***\n`;
  const senderEmbed = await groundingUtils.groundUser(to_user.id, minutes_todo, minutes_minimum, percentage, instructions);
  const embed = await groundingUtils.groundUser(to_user.id, 0, minutes_minimum, percentage, instructions, secretSetting);

  if (instructions) {
    embed.addFields({ name: 'Instructions', value: instructions});
  }

  if (personalized_instructions) {
    embed.addFields({ name: 'Personalized Instructions', value: personalized_instructions});
  }

  if (secretSetting === 'secret') {
    await interaction.editReply({
      content: 'Your grounding has been created.',
      embeds: [senderEmbed],
      ephemeral: true
    });
    await interaction.followUp({
      content: reply,
      embeds: [embed]
    });
  } else {
    await interaction.editReply({
      content: reply,
      embeds: [embed]
    });
  }
}

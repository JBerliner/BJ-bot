import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';
import { groundingUtils, groundingMessages } from '../../utils/grounding.mjs';
import {brain, ensureBrainKeyExists} from '../../utils/brain.mjs'; // You may need to modify the import path

export const data = new SlashCommandBuilder()
  .setName('unground_user')
  .setDescription('Ungrounds a user')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to unground')
    .setRequired(true))
  .addIntegerOption(option => option.setName('minutes')
    .setDescription('The number of minutes to update the grounding for')
    .setRequired(false))
  .addIntegerOption(option => option.setName('minimum_session')
    .setDescription('The minimum session duration in minutes')
    .setRequired(false))
  .addNumberOption(option => option.setName('completion_percentage')
    .setDescription('The required percentage of completion')
    .setRequired(false))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  const to_user = interaction.options.getUser('user');
  const minutes_todo = interaction.options.getInteger('minutes');
  const minutes_minimum = interaction.options.getInteger('minimum_session');
  const percentage = interaction.options.getNumber('completion_percentage');

  // make sure minimum is 12 or less
  if (!groundingUtils.testMinutes(minutes_minimum)) {
    interaction.reply({content: groundingMessages.minMinuteMessage, ephemeral: true});
    return;
  }

  // user found, proceed with ungrounding
  if (minutes_todo > 0 || minutes_minimum || percentage) {
    ensureBrainKeyExists('grounding', to_user);
    if (typeof minutes_todo === 'undefined'
      && typeof minutes_minimum === 'undefined'
      && typeof percentage === 'undefined') {
      await interaction.reply({content: 'Please enter a value for at least one field to make an update.', ephemeral: true});
      return;
    }

    // Continue with your existing logic if at least one value is defined
    // Check and update minutes_todo only if provided
    if (typeof minutes_todo !== 'undefined') {
      brain.data.grounding[to_user.id].minutes_todo = minutes_todo;
    }
    // Check and update minutes_minimum only if provided
    if (typeof minutes_minimum !== 'undefined') {
      brain.data.grounding[to_user.id].minutes_min = minutes_minimum;
    }
    // Check and update minimum_percentage only if provided
    if (typeof percentage !== 'undefined') {
      brain.data.grounding[to_user.id].minimum_percentage = percentage;
    }
    await brain.write();

    interaction.reply(`Updated grounding for <@${to_user}>: `
      + `${minutes_todo || 1}m, `
      + `min${minutes_minimum || 0}, `
      + `${percentage || 0}% required.`);
  } else {
    // Clear grounding
    await groundingUtils.clearGrounding(to_user.id, interaction.guild);
    await interaction.reply(`Removed grounding for <@${to_user.id}>.`);
  }
}

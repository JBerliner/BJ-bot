import { SlashCommandBuilder } from 'discord.js';
import { hasConsent } from '../../utils/consent.mjs';
import { groundingUtils, groundingMessages } from '../../utils/grounding.mjs';
import { addRoleByName } from '../../utils/users.mjs';
import {backfires} from '../../utils/backfire.mjs';

const grounded_role = process.env.GROUNDED_ROLE;

export const data = new SlashCommandBuilder()
  .setName('ground_user')
  .setDescription('Grounds a user')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to ground')
    .setRequired(true))
  .addIntegerOption(option => option.setName('minutes')
    .setDescription('The number of minutes to ground the user for')
    .setRequired(true))
  .addStringOption(option => option.setName('personalized_instructions')
    .setDescription('Special instructions for the grounding (free text) - please use ||spoiler text|| for anything NSFW.')
    .setRequired(false))
  .addStringOption(option => option.setName('instruction_presets')
    .setDescription('Special instructions for the grounding (preset choices)')
    .setRequired(false)
    .addChoices(
      {name: 'Kneel while completing', value: 'Kneel while completing this grounding.'},
      {name: 'Run as surprise me', value: 'Run the groundings as "surprise me" sessions until you\'ve completed this grounding.'},
      {name: 'Shame yourself after grounding', value: 'When your grounding is over, shame yourself and count your way out.'},
      {name: 'Shame after grounding + 1s slowdown for every miss.', value: 'After the grounding, give yourself a slowdown of 1s/miss. Shame yourself. Count out of the corner.'},
      {name: 'Shame for every miss.', value: 'After the grounding, shame yourself once for each miss. Count out of the corner each time.'},
    )
  )
  .addIntegerOption(option => option.setName('minimum_session')
    .setDescription('The minimum session duration in minutes')
    .setRequired(false))
  .addNumberOption(option => option.setName('completion_percentage')
    .setDescription('The required percentage of completion')
    .setRequired(false))
  .addStringOption(option => option
    .setName('secret_settings')
    .setDescription('Choose if the settings should be hidden or public.')
    .addChoices(
      { name: 'Settings Not Hidden', value: 'normal' },
      { name: 'Settings are a Secret!', value: 'secret' }
    )
  );

export async function execute(interaction) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const minutes_todo = interaction.options.getInteger('minutes');
  const instructions = interaction.options.getString('instruction_presets');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  const minutes_minimum = interaction.options.getInteger('minimum_session') || 0;
  const percentage = interaction.options.getNumber('completion_percentage') || 0;
  const secretSetting = interaction.options.getString('secret_settings') || 'normal';

  // make sure minimum is 12 or less
  if (!groundingUtils.testMinutes(minutes_minimum)) {
    interaction.reply({content: groundingMessages.minMinuteMessage, ephemeral: true});
    return;
  }


  if (await backfires(interaction.member, to_user)) {
    if (secretSetting === 'secret') {
      await interaction.deferReply({ephemeral: true});
    } else {
      await interaction.deferReply({ephemeral: false});
    }

    // add grounded role
    await addRoleByName(sender.id, interaction.guild, grounded_role);

    // update the brain accordingly and reply with what's going on
    let reply = '# Backfire!\n';
    reply += `***<@${sender.id}> tried to give <@${to_user.id}> a grounding but it backfired!***\n`;
    const embed = await groundingUtils.groundUser(sender.id, minutes_todo, minutes_minimum, percentage, instructions, secretSetting);

    if (instructions) {
      embed.addFields({ name: 'Instructions', value: instructions});
    }

    if (personalized_instructions) {
      embed.addFields({ name: 'Personalized Instructions', value: personalized_instructions});
    }

    if (secretSetting === 'secret') {
      await interaction.editReply({
        content: 'Your grounding has been created.',
        ephemeral: true
      });
      await interaction.followUp({
        content: reply,
        embeds: [embed]
      });
    } else {
      await interaction.editReply({
        content: reply,
        embeds: [embed]
      });
    }
    return;
  }

  // check consent
  if (!await hasConsent(sender, to_user)) {
    interaction.reply({
      content: 'You do not have consent to ground that user.',
      ephemeral: true
    });
    return;
  }


  if (secretSetting === 'secret') {
    await interaction.deferReply({ephemeral: true});
  } else {
    await interaction.deferReply({ephemeral: false});
  }

  // add grounded role
  await addRoleByName(to_user.id, interaction.guild, grounded_role);

  // update the brain accordingly and reply with what's going on
  const reply = `***<@${sender.id}> has given <@${to_user.id}> a grounding!***\n`;
  const embed = await groundingUtils.groundUser(to_user.id, minutes_todo, minutes_minimum, percentage, instructions, secretSetting);

  if (instructions) {
    embed.addFields({ name: 'Instructions', value: instructions});
  }

  if (personalized_instructions) {
    embed.addFields({ name: 'Personalized Instructions', value: personalized_instructions});
  }

  if (secretSetting === 'secret') {
    await interaction.editReply({
      content: 'Your grounding has been created.',
      ephemeral: true
    });
    await interaction.followUp({
      content: reply,
      embeds: [embed]
    });
  } else {
    await interaction.editReply({
      content: reply,
      embeds: [embed]
    });
  }
}

import { SlashCommandBuilder, EmbedBuilder } from 'discord.js';
import {ensureBrainKeyExistsPromise} from '../utils/brain.mjs';
import {timeUtils} from '../utils/time.mjs';
import {problemUtils} from '../utils/math_problems.mjs';
import _ from 'lodash';

export const data = new SlashCommandBuilder()
  .setName('statistics')
  .setDescription('Get various statistics.')
  .addSubcommand(subcommand =>
    subcommand
      .setName('math')
      .setDescription('Get math statistics for a user')
      .addUserOption(option =>
        option.setName('user')
          .setDescription('Whose stats you want to see?')
          .setRequired(false)
      )
  )
  .addSubcommand(subcommand =>
    subcommand
      .setName('shape_count')
      .setDescription('Get shape counting statistics for a user')
      .addUserOption(option =>
        option.setName('user')
          .setDescription('Whose stats you want to see?')
          .setRequired(false)
      )
  );

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  const userId = user ? user.id : interaction.user.id;

  if (interaction.options.getSubcommand() === 'math') {
    const stats = await ensureBrainKeyExistsPromise('tracking', 'math', userId);

    if (_.isEmpty(stats)) {
      await interaction.reply({content: `I do not have any math stats for <@${userId}>`, ephemeral: true});
      return;
    }

    const percentage = stats.test.problems_assigned > 0
      ? (stats.test.problems_correct / stats.test.problems_assigned) * 100
      : 0;
    const grade = problemUtils.calculateLetterGrade(percentage);
    const gradeText = percentage > 0 ?
      `# Overall Grade: ${grade}\n${percentage}%\n*Grades are based solely on math tests: correct problems / assigned problems.*` :
      '# Overall Grade: Incomplete\n*Start a math test to get a grade.*';

    const embed = new EmbedBuilder()
      .setColor(189729)
      .setTitle('Math Statistics')
      .setDescription(`Math statistics for ${user ? user.tag : interaction.user.tag}\n\n${gradeText}`)
      .addFields(
        getFieldData('Addition', stats.operation.addition),
        getFieldData('Subtraction', stats.operation.subtraction),
        getFieldData('Multiplication', stats.operation.multiplication),
        getFieldData('Division', stats.operation.division),
        getFieldData('Hard Mode', stats.operation.hard_mode),
        getPracticeTestData('Practice', stats.practice),
        getPracticeTestData('Test', stats.test),
        getPracticeTestData('All', stats.all)
      )
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: interaction.user.avatarURL() });

    await interaction.reply({ embeds: [embed] });
  } else if (interaction.options.getSubcommand() === 'shape_count') {
    const stats = await ensureBrainKeyExistsPromise('tracking', 'shape_count', userId);

    if (_.isEmpty(stats)) {
      await interaction.reply({content: `I do not have any shape counting stats for <@${userId}>`, ephemeral: true});
      return;
    }

    // Ensure stats.all.totalGuesses is not zero to avoid division by zero
    const successRate = stats.all.totalGuesses > 0
      ? (stats.all.successfulGuesses / stats.all.totalGuesses) * 100
      : 0;
    const grade = problemUtils.calculateLetterGrade(successRate);
    const gradeText = successRate > 0
      ? `Grade: ${grade} (${successRate.toFixed(2)}%)`
      : 'Grade: Incomplete';


    const shapeStatsEmbed = new EmbedBuilder()
      .setColor(189729)
      .setTitle('Shape Counting Statistics')
      .setDescription(`Shape counting statistics for ${user ? user.tag : interaction.user.tag}`)

      // Overall
      .addFields(getShapeCountFieldData('Overall \n' + gradeText, stats.all))
      .addFields({ name: '\u200B', value: '\u200B', inline: false })  // Separator

      // Difficulty
      .addFields(
        { name: 'Difficulty', value: '\u200B', inline: false },  // Header
        getShapeCountFieldData('Easy', stats.difficultyStats.easy),
        getShapeCountFieldData('Medium', stats.difficultyStats.medium),
        getShapeCountFieldData('Hard', stats.difficultyStats.hard)
      )
      .addFields({ name: '\u200B', value: '\u200B', inline: false })  // Separator

      // Size
      .addFields(
        { name: 'Size', value: '\u200B', inline: false },  // Header
        getShapeCountFieldData('X Small', stats.sizeStats.x_small),
        getShapeCountFieldData('Small', stats.sizeStats.small),
        getShapeCountFieldData('Medium', stats.sizeStats.medium),
        getShapeCountFieldData('Large', stats.sizeStats.large)
      )
      .addFields({ name: '\u200B', value: '\u200B', inline: false })  // Separator

      // Movement
      .addFields(
        { name: 'Movement', value: '\u200B', inline: false },  // Header
        getShapeCountFieldData('Still', stats.movementStats.still),
        getShapeCountFieldData('Blinking', stats.movementStats.fade)
      )
      .addFields({ name: '\u200B', value: '\u200B', inline: false })  // Separator

      // Color
      .addFields(
        { name: 'Color', value: '\u200B', inline: false },  // Header
        getShapeCountFieldData('Random', stats.colorStats.random),
        getShapeCountFieldData('Black', stats.colorStats.black),
        getShapeCountFieldData('White', stats.colorStats.white),
        getShapeCountFieldData('Rainbow', stats.colorStats.rainbow),
        getShapeCountFieldData('Pastel', stats.colorStats.pastel)
      )
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: interaction.user.avatarURL() });

    await interaction.reply({ embeds: [shapeStatsEmbed] });
  }
}

function getShapeCountFieldData(name, data) {
  return {
    name: name,
    value: `Total Guesses: ${data.totalGuesses}\n` +
      `Successful Guesses: ${data.successfulGuesses}\n` +
      `Failed Guesses: ${data.failedGuesses}\n` +
      `Total Time: ${timeUtils.msToTime(data.timeTaken * 60000)}\n` +
      `Average Time: ${data.averageTime.toFixed(2)} minutes`,
    inline: true
  };
}

function getFieldData(name, data) {
  return {
    name: name,
    value: `Started: ${data.started}\n` +
      `Completed: ${data.completed}\n` +
      `Correct: ${data.correct}\n` +
      `Incorrect: ${data.incorrect}`,
    inline: true
  };
}

function getPracticeTestData(name, data) {
  return {
    name: name,
    value: `Started: ${data.started}\n` +
      `Cancelled: ${data.cancelled}\n` +
      `Completed: ${data.completed}\n` +
      `Problems Assigned: ${data.problems_assigned}\n` +
      `Problems Completed: ${data.problems_completed}\n` +
      `Problems Correct: ${data.problems_correct}\n` +
      `Problems Incorrect: ${data.problems_incorrect}\n` +
      `Time Spent: ${timeUtils.msToTime(data.time)}`,
    inline: false
  };
}

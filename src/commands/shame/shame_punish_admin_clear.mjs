import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';
import {brain} from '../../utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('shame_punish_admin_clear')
  .setDescription('Clears a user\'s shame counts')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('The user to clear the shame counts for')
      .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  const user = interaction.options.getUser('user');

  if (!brain.data.shame_counts || !brain.data.shame_counts[user.id]) {
    await interaction.reply({
      content: `The user ${user.username} does not have any shame counts to clear.`,
      ephemeral: true
    });
    return;
  }

  delete brain.data.shame_counts[user.id];
  await brain.write();

  await interaction.reply({
    content: `The user ${user.username}'s shame counts have been cleared. However, their shame remains intact.`,
    ephemeral: true
  });
}

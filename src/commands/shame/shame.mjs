import { SlashCommandBuilder } from 'discord.js';
import {
  createShameRecord,
  addShameByUserId,
  shame_timer_time,
  userHasShameByID,
  addToShameCountTarget,
  getShameUserRecord,
} from '../../utils/shame.mjs';
import {backfires} from '../../utils/backfire.mjs';
import {hasConsent} from '../../utils/consent.mjs';

const shame_room = process.env.SHAME_ROOM;

export const data = new SlashCommandBuilder()
  .setName('shame')
  .setDescription('Shame a user')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('The user you want to shame')
      .setRequired(true))
  .addIntegerOption(option => option.setName('counting_target')
    .setDescription('Choose a different number for the user to count to to get out of the corner.')
    .setRequired(false))
  .addIntegerOption(option => option.setName('slowdown')
    .setDescription('One-time slow down for the corner.')
    .setRequired(false))
  .addIntegerOption(option => option.setName('mistake_penalty')
    .setDescription('Increase the target by this number if a mistake is made')
    .setRequired(false))
  ;
export async function execute(interaction) {
  await interaction.deferReply();

  // No shame from shamed users
  if (await userHasShameByID(interaction.guild, interaction.user.id)) {
    await interaction.editReply('Unless you\'re transferring it ' +
      '(which is a whole different command), you cannot ' +
      'shame someone when you are already shamed yourself!\n' +
      'However, for trying, here\'s another 50 in the Corner of Shame...');
    await addToShameCountTarget(interaction.user, 50);
    let shameRecord = await getShameUserRecord(interaction.user);
    await interaction.followUp(`You can count to **${shameRecord.count_to}** now. :heart:`);
    return;
  }

  // No more shame on shamed users
  const mentioned_user = interaction.options.getUser('user');
  if (await userHasShameByID(interaction.guild, mentioned_user.id)) {
    await interaction.editReply(`${mentioned_user} is already shamed! **Double shame!** \n` +
      'But no, you can\'t actually shame them more.');
    return;
  }

  const count_to = interaction.options.getInteger('counting_target');
  let count_reply = '';
  if (count_to) {
    count_reply = `\n**Now count to ${count_to} in <#${shame_room}>!**`;
  }

  const slowdown = interaction.options.getInteger('slowdown');
  const mistake_penalty = interaction.options.getInteger('mistake_penalty');

  let slowdown_reply = slowdown ? `\nOne-time slowdown: ${slowdown} seconds` : '';
  let mistake_penalty_reply = mistake_penalty ? `\nMistake penalty: ${mistake_penalty}` : '';

  const channel = interaction.channel.id;

  // Check if user tries to shame the bot itself
  if (mentioned_user.id === interaction.client.user.id) {
    await interaction.editReply('Nice try. Go to the corner for that!');
    await createShameRecord(interaction.user, 0, shame_timer_time, 'shaming_bj', count_to, null, channel, slowdown, mistake_penalty);
    await addShameByUserId(interaction.guild, interaction.user.id);
    await interaction.followUp(`SHAME ON <@${interaction.user.id}>!` + count_reply + slowdown_reply + mistake_penalty_reply);
    return;
  }

  if (mentioned_user.bot) {
    await interaction.editReply("I don't allow bot-shaming around these parts.");
    await createShameRecord(interaction.user, 0, shame_timer_time, 'bot_shaming', count_to, null, channel, slowdown, mistake_penalty);
    await addShameByUserId(interaction.guild, interaction.user.id);
    await interaction.followUp(`SHAME ON <@${interaction.user.id}>!` + count_reply + slowdown_reply + mistake_penalty_reply);
    return;
  }

  if (await backfires(interaction.member, mentioned_user)) {
    let reply = '# Backfire!\n';
    reply += `<@${interaction.user.id}> tried to shame <@${mentioned_user.id}> but it backfired!`;
    await createShameRecord(interaction.user, 0, shame_timer_time, 'shame_backfire', count_to, interaction.user.id, channel, slowdown, mistake_penalty);
    await addShameByUserId(interaction.guild, interaction.user.id);
    await interaction.editReply(reply + count_reply + slowdown_reply + mistake_penalty_reply);
    return;
  }

  if (await hasConsent(interaction.user, mentioned_user)) {
    await createShameRecord(mentioned_user, 0, shame_timer_time, 'shame_user', count_to, interaction.user.id, channel, slowdown, mistake_penalty);
    await addShameByUserId(interaction.guild, mentioned_user.id);
    await interaction.editReply(`SHAME ON <@${mentioned_user.id}>!` + count_reply + slowdown_reply + mistake_penalty_reply);
  } else {
    await interaction.editReply('You do not have consent to shame that user.');
  }
}

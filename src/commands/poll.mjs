import { SlashCommandBuilder, EmbedBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('poll')
  .setDescription('Create a poll with up to 20 choices.')
  .addStringOption(option =>
    option.setName('message')
      .setDescription('The poll question')
      .setRequired(true))
  .addStringOption(option =>
    option.setName('choice1')
      .setDescription('Choice 1')
      .setRequired(true))
  .addStringOption(option =>
    option.setName('choice2')
      .setDescription('Choice 2')
      .setRequired(true));

// Adding 18 more optional choices
for (let i = 3; i <= 20; i++) {
  data.addStringOption(option =>
    option.setName(`choice${i}`)
      .setDescription(`Choice ${i}`)
      .setRequired(false));
}

export async function execute(interaction) {
  const message = interaction.options.getString('message');
  const choices = [];

  for (let i = 1; i <= 20; i++) {
    const choice = interaction.options.getString(`choice${i}`);
    if (choice) choices.push(choice);
  }

  const choicesString = choices.map((choice, index) =>
    `${String.fromCodePoint(0x1F1E6 + index)} ${choice}`
  ).join('\n\n');


  const embed = new EmbedBuilder()
    .setColor(0x0099FF)
    .setTitle('Poll')
    .setDescription(message)
    .addFields({ name: 'Choices', value: choicesString })
    .setFooter({ text: `Poll by ${interaction.member.displayName} • ${new Date().toLocaleString('en-US', { month: 'numeric', day: 'numeric', year: 'numeric', hour: '2-digit', minute: '2-digit' })}` });

  const sentMessage = await interaction.reply({ embeds: [embed], fetchReply: true });

  // Add reactions to the sent message for voting using Unicode characters for regional indicator letters
  for (let i = 0; i < choices.length; i++) {
    await sentMessage.react(String.fromCodePoint(0x1F1E6 + i)); // Unicode range for regional indicator symbols A through Z
  }
}

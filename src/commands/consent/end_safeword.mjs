import { SlashCommandBuilder } from 'discord.js';
import { endSafeword } from '../../utils/consent.mjs';

export const data = new SlashCommandBuilder()
  .setName('end_safeword')
  .setDescription('Deactivate your safeword and restore your consent settings');

export async function execute(interaction) {
  if (!await endSafeword(interaction.user)) {
    await interaction.reply('Your safeword wasn\'t set, so nothing has changed.');
  } else {
    await interaction.reply('Okay. Your consent settings are still as you set them ' +
      'but people are allowed to mess with you again if you\'ve given them consent.');
  }
}

import { SlashCommandBuilder } from 'discord.js';
import {getAndSortReceivedConsent} from '../../utils/consent.mjs';


export const data = new SlashCommandBuilder()
  .setName('consent_received')
  .setDescription('See who all you have consent from!')

export async function execute(interaction) {
  await interaction.reply('Give me a moment to organize the names...');

  const result = await getAndSortReceivedConsent(interaction.user, interaction.guild);

  if (result.hasConsent) {
    await interaction.editReply(`You've received consent from:\n${result.list}`);
  } else {
    await interaction.editReply('You haven\'t received consent from anyone yet.');
  }
}

import { SlashCommandBuilder } from 'discord.js';
import {hasConsent} from '../../utils/consent.mjs';
import {pingsEveryone} from '../../utils/security.mjs';
import {addShameByUserId, createShameRecord} from '../../utils/shame.mjs';
import {brain, ensureBrainKeyExists} from '../../utils/brain.mjs';
import {clearUserMessages, countUserMessages} from '../../utils/failed_count_msg.mjs';

export const data = new SlashCommandBuilder()
  .setName('failed_count_message_add')
  .setDescription('Add a message that has a chance to show when someone messes up the count.')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('User to assign the message to')
      .setRequired(true))
  .addStringOption(option =>
    option.setName('message')
      .setDescription('The message to show')
      .setRequired(true))
  .addBooleanOption(option =>
    option.setName('one_time')
      .setDescription('Whether the message is one-time or not')
      .setRequired(false))
  .addBooleanOption(option =>
    option.setName('overwrite')
      .setDescription('Whether to overwrite ALL your existing messages')
      .setRequired(false));

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  const message = interaction.options.getString('message');
  const one_time = interaction.options.getBoolean('one_time') || false;
  const override = interaction.options.getBoolean('override') || false;

  let mentioned_user_id = user.id;
  let sender = interaction.user; // User object

  if (!await hasConsent(sender, user)) {
    await interaction.reply({
      content: 'You do not have consent to add counting messages to that user.',
      ephemeral: true
    });
    return;
  }

  // add to brain
  let id = (new Date()).getTime();

  if (pingsEveryone(message)) {
    await interaction.reply('Fuck you. And SHAME on you for trying to ping everyone!');
    await addShameByUserId(interaction.guild, interaction.user.id);
    await createShameRecord(interaction.user, 0, 86400000, 'mass_ping');
    return;
  }

  await interaction.deferReply({ephemeral: true});

  // Create brain object if needed and get brain object
  let messages = ensureBrainKeyExists('count_fail_msgs', mentioned_user_id);

  // Check if the message already exists for the user
  for (let msgId in messages) {
    if (messages[msgId].text === message) {
      await interaction.editReply({
        content: 'This message already exists for the user.',
        ephemeral: true
      });
      return;
    }
  }

  if (override) {
    // If using "set" instead of "add" overwrite all other messages this top set
    await clearUserMessages(mentioned_user_id, sender.id);
  }

  // Set brain object information
  messages[id] = {
    'id': id,
    'is_one_time': one_time,
    'text': message,
    'date_set': new Date(),
    'set_by': sender.id,
    'given_to': mentioned_user_id,
    'given_count': 0,
    'dm_only': false // TODO: allow for DM messages
  }
  // Save!
  await brain.write();
  // How many messages are set?
  let count = countUserMessages(mentioned_user_id);

  let reply_text = "I've added the counting message.\n" +
    `<@${mentioned_user_id}> currently has ${count} counting message(s) set.`;
  if (count > 1) {
    reply_text += ' One will be chosen randomly if they mess up the count.';
  }

  await interaction.editReply(reply_text);
}

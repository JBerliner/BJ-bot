import { SlashCommandBuilder } from 'discord.js';
import {hasConsent} from '../../utils/consent.mjs';
import {pingsEveryone} from '../../utils/security.mjs';
import {addShameByUserId, createShameRecord} from '../../utils/shame.mjs';
import {brain, ensureBrainKeyExists} from '../../utils/brain.mjs';
import {countUserMessages} from '../../utils/failed_count_msg.mjs';

export const data = new SlashCommandBuilder()
  .setName('failed_count_message_add_multi')
  .setDescription('Add multiple messages that have a chance to show when someone messes up the count.')
  .addStringOption(option =>
    option.setName('users')
      .setDescription('User IDs to assign the message to, separated by semicolons')
      .setRequired(true))
  .addStringOption(option =>
    option.setName('messages')
      .setDescription('The messages to show, separated by semicolons')
      .setRequired(true))
  .addBooleanOption(option =>
    option.setName('one_time')
      .setDescription('Whether the messages are one-time or not')
      .setRequired(false));

export async function execute(interaction) {
  const usersString = interaction.options.getString('users');
  const users = usersString.split(';').map(id => id.trim());

  const messagesString = interaction.options.getString('messages');
  const messages = messagesString.split(';').map(msg => msg.trim().replace(/\\n/g, '\n'));

  const one_time = interaction.options.getBoolean('one_time') || false;

  let sender = interaction.user; // User object
  await interaction.deferReply({ephemeral: true});
  await interaction.editReply('Adding Messages');

  for (let userId of users) {
    let user = await interaction.client.users.fetch(userId.trim());
    if (!user) {
      continue; // skip if user not found, you can add error message if you want
    }

    for (let message of messages) {
      if (await hasConsent(sender, user)) {
        let mentioned_user_id = user.id;
        let id = (new Date()).getTime();

        if (pingsEveryone(message)) {
          await interaction.followUp('Fuck you. And SHAME on you for trying to ping everyone!');
          await addShameByUserId(interaction.guild, interaction.user.id);
          await createShameRecord(interaction.user, 0, 86400000, 'mass_ping');
          return;
        }

        // add to brain
        // Create brain object if needed and get brain object
        let existingMessages = ensureBrainKeyExists('count_fail_msgs', mentioned_user_id);

        // Check if the message already exists for the user
        let messageExists = false;
        for (let msgId in existingMessages) {
          if (existingMessages[msgId].text === message) {
            await interaction.followUp({
              content: `This message already exists for ${user}.`,
              ephemeral: true
            });
            messageExists = true;
            break;
          }
        }

        if (messageExists) {
          continue;
        }

        // Set brain object information
        existingMessages[id] = {
          'id': id,
          'is_one_time': one_time,
          'text': message,
          'date_set': new Date(),
          'set_by': sender.id,
          'given_to': mentioned_user_id,
          'given_count': 0,
          'dm_only': false // TODO: allow for DM messages
        }
        // Save!
        await brain.write();

        // How many messages are set?
        let count = countUserMessages(mentioned_user_id);

        let reply_text = "I've added the counting message.\n" +
          `> ${message}\n` +
          `<@${mentioned_user_id}> currently has ${count} counting message(s) set.`;
        if (count > 1) {
          reply_text += ' One will be chosen randomly if they mess up the count.';
        }

        await interaction.followUp({content: reply_text, ephemeral: true});
      } else {
        await interaction.followUp({content: `You don't have consent for ${user}`, ephemeral: true});
      }
    }
  }
  await interaction.followUp({content: 'I\'ve added what I can.', ephemeral: true});
}

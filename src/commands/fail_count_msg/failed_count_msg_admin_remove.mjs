import { PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { brain } from '../../utils/brain.mjs';
import { findMessageById } from '../../utils/failed_count_msg.mjs';

export const data = new SlashCommandBuilder()
  .setName('failed_count_msg_admin_remove')
  .setDescription('Remove a failed count message by ID, regardless of who set it')
  .addStringOption(option =>
    option.setName('message_id')
      .setDescription('The ID of the message to remove')
      .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  await interaction.deferReply({ ephemeral: true });

  const messageId = interaction.options.getString('message_id');

  let found_message = findMessageById(messageId);

  if (found_message) {
    delete brain.data.count_fail_msgs[found_message.given_to][messageId]; // Works
    await brain.write();
    await interaction.editReply({content: 'Message removed successfully.', ephemeral: true});
  } else {
    await interaction.editReply({content: 'I couldn\'t find that message.', ephemeral: true});
  }
}

/* eslint-env jest */

import {
  getFirstUserIdFromMentionOnRespond,
  getFirstUserIdFromMentionOnHear,
  addRoleByName,
  removeRoleByName,
  getGuildMemberByUserID,
  getRoleByName,
} from '../../utils/users.mjs';

import {
  getMessageMock,
  getGuildMock,
  getRoleMock,
  getGuildMemberMock,
} from '../../utils/mocks.mjs';

const userId = '123456789';

describe('users utility functions', () => {
  test('getFirstUserIdFromMentionOnRespond Via DM', () => {
    const messageMock = getMessageMock();
    messageMock.content = `<@${userId}>`;
    messageMock.mentions.users.first.mockReturnValue(null);
    messageMock.guildId = null;

    const result = getFirstUserIdFromMentionOnRespond(messageMock);

    expect(result).toEqual(userId);
  });

  test('getFirstUserIdFromMentionOnRespond Public Message', () => {
    const messageMock = getMessageMock();
    messageMock.content = `<@${userId}>`;
    messageMock.mentions.users.first.mockReturnValue(null);
    const result = getFirstUserIdFromMentionOnRespond(messageMock);
    expect(result).toEqual(userId);
  });

  test('getFirstUserIdFromMentionOnHear', () => {
    const messageMock = getMessageMock();
    messageMock.content = `<@${userId}>`;

    // test it where it can't find mentions, but there is one.
    let result = getFirstUserIdFromMentionOnHear(messageMock);
    expect(result).toEqual(userId);

    // Now make sure there's a mention there, and test that return
    messageMock.mentions.users.mockReturnValue(null);
    messageMock.guildId = null;

    result = getFirstUserIdFromMentionOnHear(messageMock);
    expect(result).toEqual(userId);
  });

  test('addRoleByName and removeRoleByName', async () => {
    const roleName = 'Role Name';
    const guild = getGuildMock();
    const role = getRoleMock();
    const member = getGuildMemberMock();

    guild.roles.cache.find.mockReturnValue(role);
    guild.members.fetch.mockResolvedValue(member);

    await addRoleByName(userId, guild, roleName);

    expect(guild.roles.cache.find).toHaveBeenCalledWith(expect.any(Function));
    expect(guild.members.fetch).toHaveBeenCalledWith(userId);
    expect(member.roles.add).toHaveBeenCalledWith(role);

    // Check to see if the role was actually put on the guild member
    expect(member.roles.cache.has(role.id)).toBe(true);

    // Now remove the role
    await removeRoleByName(userId, guild, roleName);

    expect(guild.roles.cache.find).toHaveBeenCalledWith(expect.any(Function));
    expect(guild.members.fetch).toHaveBeenCalledWith(userId);
    expect(member.roles.remove).toHaveBeenCalledWith(role);

    // Check to see if the role was actually removed from the guild member
    expect(member.roles.cache.has(role.id)).toBe(false);
  });

  test('getGuildMemberByUserID', async () => {
    const guild = getGuildMock();
    const member = getGuildMemberMock();

    guild.members.fetch.mockResolvedValue(member);

    const result = await getGuildMemberByUserID(guild, userId);
    // make sure
    expect(guild.members.fetch).toHaveBeenCalledWith(userId);
    expect(result).toEqual(member);
  });

  test('getRoleByName', async () => {
    const roleName = 'Role Name';
    const guild = getGuildMock();
    const role = getRoleMock();

    guild.roles.cache.find.mockReturnValue(role);

    const result = await getRoleByName(guild, roleName);

    expect(guild.roles.cache.find).toHaveBeenCalledWith(expect.any(Function));
    expect(result).toEqual(role);
  });
});

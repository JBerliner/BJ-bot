import {ensureBrainKeyExistsPromise, brain} from '../../utils/brain.mjs';

import {generateNewProblem, getRandomTimesTableProblem, problemUtils} from '../../utils/math_problems.mjs';
import {timeUtils} from '../../utils/time.mjs';
import {track} from '../../utils/track.mjs';

export const handleMathProblems = {
  name: 'handleMathProblems',
  async execute(message) {
    // Make sure the 'problems' key exists
    await ensureBrainKeyExistsPromise('problems');

    // Make sure the user has problems assigned and this is the right channel
    if (brain.data.problems[message.author.id] && message.channelId === brain.data.problems[message.author.id].channel) {
      const data = brain.data.problems[message.author.id];
      const tracking = await track.setUpMathTracking(message.author.id);

      // Before anything else, check for cancellation
      if (message.content.toLowerCase() === 'cancel') {
        // Cancel command given, cancel this session
        message.reply('Problems cancelled. This channel will be deleted in 10 seconds!');
        tracking.all.cancelled++;
        tracking[data.testing].cancelled++
        await brain.write();

        // Send the detailed cancellation message
        await sendCancelledMessage(message, data);

        // Delete the channel in 10 seconds
        setTimeout(function() {
          if (message.guild.channels.cache.get(message.channelId) !== undefined) {
            message.channel.delete();
          }
        }, 10000);
        return;
      }

      // Always save last time something was entered
      data.last_time = Date.now();

      // Check to see if the reply is the answer to the problem
      const userAnswerStr = message.content.replace(/,/g, ''); // Remove any commas
      const userAnswer = parseFloat(userAnswerStr);
      const correctAnswer = data.currentAnswer;

      const isTestMode = data.testing === 'test';
      const isAnswerCorrect = userAnswer === correctAnswer;

      // Before much else, is content 'start'?
      if (message.content.toLowerCase() === 'start' && data.started === false) {
        tracking.all.started++;
        tracking[data.testing].started++;
        data.started = true;
        data.first_problem_start = Date.now();
        await brain.write();

        // Give the first problem
        const newProblem = await getNewProblem(data);
        message.reply(`**#${data.problems_completed + 1}.**\n  ${newProblem.problem}`);
        tracking.operation[newProblem.operation].started++;
        await brain.write();
        if (newProblem.operation === 'division') {
          message.channel.send('Round to two decimals! Use US notation with \'.\' denoting the decimal.');
        }
        return;
      }

      if (data.started === false) {
        message.reply('When you\'re ready you can turn your test over by sending \n> start');
        return;
      }

      // Only increment problems completed if the answer is correct or in test mode
      if (isAnswerCorrect || isTestMode) {
        data.problems_completed += 1;
        tracking.operation[data.currentOperation].completed++;
        tracking[data.testing].problems_completed++;
        tracking.all.problems_completed++;
      }

      if (isAnswerCorrect) {
        data.correct++;
        tracking.operation[data.currentOperation].correct++;
        tracking[data.testing].problems_correct++;
        tracking.all.problems_correct++;
      }

      if (!isAnswerCorrect) {
        // Always update the mistakes when the answer is wrong
        data.mistakes++;
        data.mistake_on_problem++;
        tracking.operation[data.currentOperation].incorrect++;
        tracking[data.testing].problems_incorrect++;
        tracking.all.problems_incorrect++;
      }

      if (isAnswerCorrect && !isTestMode) {
        await message.react('✅');
      } else if (isTestMode && !isAnswerCorrect) {
        // Store the wrong answer, the entered answer, and the actual answer
        if (!data.wrong_test_problems) {
          data.wrong_test_problems = [];
        }

        data.wrong_test_problems.push({
          problem: data.currentProblem,
          enteredAnswer: userAnswerStr,
          correctAnswer: correctAnswer
        });
      }

      await brain.write();

      // Check if they are done
      if (data.problems_completed >= data.problems_todo) {
        await sendSuccessMessage(message, data);
        setTimeout(function() {
          if (message.guild.channels.cache.get(message.channelId) !== undefined) {
            message.channel.delete();
          }
        }, 10000);
        return;
      }

      // Not done
      if (isAnswerCorrect || isTestMode) {
        // Give a new problem if the answer was correct OR we're in test mode
        const newProblem = await getNewProblem(data);
        message.reply(`**#${data.problems_completed + 1}.**\n  ${newProblem.problem}`);
        tracking.operation[newProblem.operation].started++;
        await brain.write();
        if (newProblem.operation === 'division') {
          message.channel.send('Round to two decimals! Use US notation with \'.\' denoting the decimal.');
        }
      } else {
        message.react('❌');
        // If it's not, tell the user it's wrong and to keep trying
        message.reply('That is not the correct answer. Please try again.');
      }
    }
  },
}

async function getNewProblem(data) {
  let newProblem;
  if (data.setting === 'times_table') {
    newProblem = await getRandomTimesTableProblem(data.times_tables_problems_remaining);
    data.currentOperation = 'multiplication';
  } else {
    newProblem = generateNewProblem(data.operation);
    data.currentOperation = newProblem.operation;
  }
  data.currentProblem = newProblem.problem;
  data.currentAnswer = newProblem.answer;
  data.mistake_on_problem = 0;
  await brain.write();
  return newProblem;
}

async function sendSuccessMessage(message, data) {
  const timeTaken = Date.now() - data.set_time;
  const timeRecorded = Date.now() - data.first_problem_start;
  const delayToStart = timeRecorded - timeTaken;

  await updateTracking(message.author.id, data, 'completed');

  message.reply('Congratulations! You have completed your math assignment after ' +
    `${timeUtils.msToTime(timeTaken)}! This channel will be deleted in 10 seconds.`);
  let numMistakes = data.mistakes;
  const chan = message.client.channels.cache.get(data.cmd_channel);
  let reply_msg = '';

  if (data.set_by_id !== message.author.id) {
    reply_msg += `<@${message.author.id}> completed their math assignment for <@${data.set_by_id}>. `;
  } else {
    reply_msg += `<@${message.author.id}> completed their math assignment.`;
  }

  reply_msg += ` They made **${numMistakes}** ${numMistakes === 1 ? 'mistake' : 'mistakes'} ` +
    `and it took them **${timeUtils.msToTime(timeTaken)}** from time assigned, ` +
    `or **${timeUtils.msToTime(timeRecorded)}** from time started.\n\n`;

  if (delayToStart > (5 * 60 * 1000)) {
    reply_msg += `It took **${timeUtils.msToTime(delayToStart)}** to get started. ` +
      'Can you explain to us what took you so long to get started??\n\n';
  }

  // If in test mode, show the incorrect answers
  if (data.testing === 'test') {
    // Assuming the total number of problems is stored in data.problems_todo
    let totalQuestions = data.problems_todo;
    let correctAnswers = data.correct;
    let percentageGrade = (correctAnswers / totalQuestions) * 100; // Percentage grade
    let letterGrade = problemUtils.calculateLetterGrade(percentageGrade);

    // Send the grade
    reply_msg += `Your grade is: **${letterGrade}** (${percentageGrade.toFixed(2)}%)\n\n`;

    // If there are any incorrect answers, show them
    if (data.wrong_test_problems && data.wrong_test_problems.length > 0) {
      reply_msg += constructWrongProblemsMessage(data.wrong_test_problems);
    }
  }
  try {
    await chan.send(reply_msg);
  } catch (error) {
    let botNoiseChannel = message.client.channels.cache.get(process.env.BOTNOISE_CHANNEL_ID);
    botNoiseChannel.send("An error occurred while sending a message to the intended channel. Here's the message content:\n\n" + reply_msg);
  }

  await problemUtils.clearProblems(message.author.id);
}

async function sendCancelledMessage(message, data) {
  const chan = message.client.channels.cache.get(data.cmd_channel);

  await updateTracking(message.author.id, data, 'cancelled');

  // Calculate problems completed and those left
  let completedProblems = data.problems_completed;
  let totalProblems = data.problems_todo;
  let numMistakes = data.mistakes;
  let correctAnswers = data.correct;

  let reply_msg = `<@${message.author.id}> turned in their math ${data.testing} early! ` +
    `Out of **${totalProblems}** problems, they completed **${completedProblems}** problems, ` +
    `getting **${correctAnswers}** right and **${numMistakes}** wrong.\n`;

  // If there are any incorrect answers, show them
  if (data.wrong_test_problems && data.wrong_test_problems.length > 0) {
    reply_msg += constructWrongProblemsMessage(data.wrong_test_problems);
  }

  chan.send(reply_msg);
  await problemUtils.clearProblems(message.author.id);
}

async function updateTracking(user_id, data, completion) {
  const timeRecorded = Date.now() - data.first_problem_start;

  const tracking = await track.setUpMathTracking(user_id);

  tracking.all.time = Number(tracking.all.time) + Number(timeRecorded);
  tracking[data.testing].time = Number(tracking[data.testing].time) + Number(timeRecorded);

  tracking.all[completion]++;
  tracking[data.testing][completion]++;
  tracking.all.completed++

  await brain.write();
}

function constructWrongProblemsMessage(wrong_test_problems) {
  let msg = '**Here are the problems you got wrong:**\n';
  for (const wrongProblem of wrong_test_problems) {
    msg += `* Problem: ${wrongProblem.problem} | Your answer: ${wrongProblem.enteredAnswer} | Correct answer: ${wrongProblem.correctAnswer}\n`;
  }
  return msg;
}

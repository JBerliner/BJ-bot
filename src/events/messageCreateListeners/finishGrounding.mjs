import {initBrain, brain, ensureBrainKeyExistsPromise} from '../../utils/brain.mjs';
import { groundingUtils } from '../../utils/grounding.mjs';
import {track} from '../../utils/track.mjs';


/**
 * This tracks the user's completion of their grounding.
 * It listens for the grounding bot messages, and checks if the user has
 * completed their minimum required completion (percentage),
 * and minimum required minutes.
 *
 * If the user has not completed their grounding,
 * it displays the amount of minutes they have completed
 * and the amount they still have to go.
 *
 * If the user has completed their grounding,
 * it removes the grounding role and sends a message
 * to the user saying their grounding is over.
 */
export const finishGrounding = {
  name: 'finishGrounding',
  async execute(message) {
    // only listen to grounding bot messages
    if (message.author.id !== '1029920668428554312') return;

    // grounding bot has to mention someone. If no one's mentioned, go no further
    let mentions = message.mentions;
    if (!(mentions && mentions.users && mentions.users.size > 0)) return;

    // Initialize brain
    await initBrain();

    let user = mentions.users.first(); // User object

    let content = message.content;
    let minutes_completed = content.match(/has completed a \d+ minute grounding/gmi);
    minutes_completed = +minutes_completed[0].match(/\d+/)[0];

    let correct_clicks_match = content.match(/Correct clicks:\s*(\d+)/i);
    let correct_clicks = correct_clicks_match ? parseInt(correct_clicks_match[1], 10) : 0;

    let incorrect_clicks_match = content.match(/Incorrect clicks:\s*(\d+)/i);
    let incorrect_clicks = incorrect_clicks_match ? parseInt(incorrect_clicks_match[1], 10) : 0;

    let missed_clicks_match = content.match(/Missed clicks:\s*(\d+)/i);
    let missed_clicks = missed_clicks_match ? parseInt(missed_clicks_match[1], 10) : 0;

    // Update the tracking data
    await track.updateGroundingTracking(user.id, {
      minutesCompleted: minutes_completed,
      correctClicks: correct_clicks,
      incorrectClicks: incorrect_clicks,
      missedClicks: missed_clicks
    });

    // Does the user have grounding settings? No grounding settings, no continue
    if (!brain.data.grounding[user?.id]) return;

    // user has grounding settings
    // Let's do this!
    let grounding = brain.data.grounding[user.id];
    let completion = content.match(/Completion: \d+\.?\d*%/gmi);
    completion = +completion[0].match(/\d+\.?\d*/)[0];
    let fail = false;
    let reply = '';

    if (grounding.minimum_percentage > 0 && +grounding.minimum_percentage > +completion) {
      reply += 'You didn\'t complete your minimum ';
      reply += `required ${grounding.minimum_percentage}% completion. `;
      fail = true;
    }

    if (grounding.minutes_min > 0 && +grounding.minutes_min > minutes_completed) {
      reply += 'You didn\'t complete the required minimum ';
      reply += `of ${grounding.minutes_min} minutes. `;
      fail = true;
    }

    if (fail) {
      if (grounding.secret === 'secret') {
        // reset reply for secret groundings
        reply = 'You didn\'t hit one or more of your minimums.\n Too bad!! (So sad)\n';
      }
      reply = 'Oh no!! ' + reply + ' This grounding doesn\'t count =(';
      message.reply(reply);
    } else {
      // You didn't fail! Good for you.
      brain.data.grounding[user.id].minutes_completed =
        +brain.data.grounding[user.id].minutes_completed + minutes_completed;
      if (brain.data.grounding[user.id].minutes_completed >= brain.data.grounding[user.id].minutes_todo) {
        // you're free!
        await groundingUtils.clearGrounding(user.id, message.guild);
        message.reply('Your grounding is finally over.');
      } else {
        // not done yet
        let minutesCompleted = brain.data.grounding[user.id].minutes_completed;
        let minutesTodo = brain.data.grounding[user.id].minutes_todo;
        let reply = `You have completed ${minutesCompleted} ${minutesCompleted === 1 ? 'minute' : 'minutes'}. `;
        let togo = minutesTodo - minutesCompleted;

        if (grounding.secret === 'secret') {
          reply += '**Good luck and get to it!** ';
        } else {
          reply += `**Only ${togo} ${togo === 1 ? 'minute' : 'minutes'} of grounding left to go!**\n` +
            'Use `/grounding_check` to check the status of your grounding.';
        }
        message.reply(reply);
      }
      await brain.write();
    }
  }
}

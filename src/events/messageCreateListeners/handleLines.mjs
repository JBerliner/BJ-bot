import { brain, ensureBrainKeyExistsPromise } from '../../utils/brain.mjs';
import { lineUtils } from '../../utils/lines.mjs';
import { timeUtils } from '../../utils/time.mjs';
import {addShameByUserId, createShameRecord} from '../../utils/shame.mjs';
import Diff from 'text-diff';

export const handleLines = {
  name: 'handleLines',
  async execute(message) {
    // make sure the 'lines' key exists
    await ensureBrainKeyExistsPromise('lines');

    // Check if the author of the message has any "lines" data stored in the brain
    // and if the message was sent in the same channel as the lines were started in
    if (brain.data.lines[message.author.id] && message.channelId === brain.data.lines[message.author.id].channel) {
      const data = brain.data.lines[message.author.id];

      // before anything else, check for cancelled lines
      if (message.content.toLowerCase() === 'cancel') {
        // Cancel command given, cancel this session
        message.reply('Lines cancelled. This channel will be deleted in 10 seconds!');
        await lineUtils.clearLines(message.author.id);
        const chan = message.client.channels.cache.get(data.cmd_channel);
        chan.send(`<@${message.author.id}> cancelled their lines.`);
        // Delete the channel in 10 seconds
        setTimeout(function() {
          if (message.guild.channels.cache.get(message.channelId) !== undefined) {
            message.channel.delete();
          }
        }, 10000);
        return;
      }

      // Check for copy/pasting cheating
      if (lineUtils.isCopyPasting(message.content)) {
        let alert = `SHAME on <@${message.author.id}> for trying to copy/paste their lines`;
        alert += data.set_by_id !== message.author.id ? ` while doing lines for <@${data.set_by_id}>!` : '!!';
        message.reply('CHEATER! Shame on you!\nYou were caught copy/pasting your lines!!!');
        await handleCheatingShame(message, alert);
        message.react('❌');
        return;
      }

      // Check for typing unusually fast
      const cpm = lineUtils.calculateCPM(message.content, data.last_time);
      if (lineUtils.isTypingUnusuallyFast(cpm)) {
        let alert = `SHAME on <@${message.author.id}> for typing impossibly fast`;
        alert += data.set_by_id !== message.author.id ? ` while doing lines for <@${data.set_by_id}>!` : '!!';
        message.reply('CHEATER! Shame on you!\\nYou were caught typing impossibly fast!!!');
        await handleCheatingShame(message, alert);
        message.react('❌');
        return;
      }

      // Check if the line was typed correctly
      if (message.content === data.line) {
        // Line typed is correct
        message.react('✅');

        // Increment the number of lines completed by the user and save to the brain
        data.lines_completed += 1;
        await brain.write();

        if (!data.started) {
          data.started = true;
        } else {
          data.current_cpm += lineUtils.calculateCPM(message.content, data.last_time);
        }
        data.last_time = Date.now();

        // Check to see if user is finished with the lines
        if (data.lines_completed >= data.lines_todo) {
          // Calculate how long it took for the lines to be completed
          let msTaken = Date.now() - data.start_time;
          // The user has successfully completed their lines
          await sendSuccessMessage(message, data, msTaken);

          // Delete the channel in 10 seconds
          setTimeout(function() {
            if (message.guild.channels.cache.get(message.channelId) !== undefined) {
              message.channel.delete();
            }
          }, 10000);
          return;
        }

        // Lines aren't finished
        const newLine = await lineUtils.getNextLine(message.author.id);
        data.mistake_on_line = 0;
        await brain.write();
        let reply_line = `Your new line is: \n **${newLine.displayLine}**`;
        if (!data.hide_number_completed) {
          message.reply(`**Line ${(data.lines_completed + 1)} / ${data.lines_todo}**\n` + reply_line);
        } else {
          message.reply(reply_line);
        }
      } else {
        // Line was incorrect
        message.react('❌');
        // We want both random and buggy to trigger this.
        if (data.random_case !== 'normal') {
          data.line = lineUtils.randomize_case(data.line, 0.6);
          message.reply(`Don't mess up. Now the line is \n**${lineUtils.anticheatFilter(data.line)}**`);
        }
        data.mistakes += 1;
        data.mistake_on_line += 1;
        await lineUtils.addLinesToDo(message.author.id, data.penalty_lines);
        await brain.write();

        if (data.mistake_on_line > 2 && data.random_case === 'normal') {
          let hint = generateHint(data.line, message.content);
          message.channel.send(hint);
        }
        if (!data.started) {
          data.started = true;
        } else {
          data.current_cpm += lineUtils.calculateCPM(message.content, data.last_time);
        }
        data.last_time = Date.now();
      }
    }
  }
}

async function handleCheatingShame(message, alert_text) {
  // flag this as having cheated
  brain.data.lines[message.author.id].hasCheated = true;
  await brain.write();
  // add the shame role
  await addShameByUserId(message.guild, message.author.id);
  // add shame record for 24 hours
  await createShameRecord(message.author, 0, 86400000, 'cheating_on_lines');
  // send alert
  const channel_id = brain.data.lines[message.author.id].cmd_channel;
  const chan = message.client.channels.cache.get(channel_id);
  await chan.send(alert_text);
}

function generateHint(expectedLine, givenLine) {
  if (expectedLine === null ||
    givenLine === null ||
    expectedLine === '' ||
    givenLine === '' ||
    expectedLine === undefined ||
    givenLine === undefined
  ) {
    console.log('expected line', expectedLine);
    console.log('given line', givenLine);
    throw new Error('Expected line and given line must not be empty.');
  }

  let hint = 'Hint: ';
  // Find differences in the lines
  let diff = new Diff();
  let textDiff = diff.main(givenLine, expectedLine);
  for (let i = 0; i < textDiff.length; i++) {
    let op = textDiff[i][0]; // Operation: -1 = delete, 1 = add, 0 = no change
    let data = textDiff[i][1];
    switch (op) {
      // Nothing needs to change in this section
      case 0: {
        hint += data;
        break;
      }
      // Extra character / space
      case -1: {
        hint += '~~**' + data + '**~~';
        break;
      }
      // Character or space missing.
      case 1: {
        const newdata = replaceAll(data, ' ', '_');
        hint += '**' + newdata + '**';
        break;
      }
      default:
        break;
    }
  }
  return lineUtils.anticheatFilter(hint);
}


async function sendSuccessMessage(message, data, timeTaken) {
  message.reply(`Congratulations! You have successfully completed your lines after ${timeUtils.msToTime(timeTaken)}! This channel will be deleted in 10 seconds.`);
  let numMistakes = data.mistakes;
  await lineUtils.clearLines(message.author.id);
  const chan = message.client.channels.cache.get(data.cmd_channel);
  let reply_msg = '';
  if (data.set_by_id !== message.author.id) {
    reply_msg += `<@${message.author.id}> completed their lines for <@${data.set_by_id}>. `
  } else {
    reply_msg += `<@${message.author.id}> completed their lines.`;
  }
  reply_msg += ` They made **${numMistakes}** ${numMistakes === 1 ? 'mistake' : 'mistakes'} and it took them **${timeUtils.msToTime(timeTaken)}** ${data.lines_todo > 1 ? 'with a typing speed of **' + lineUtils.calculateFinalWPM(data.current_cpm, data.lines_todo - 1) + ' WPM!**' : '.'}`;
  if (data.hasCheated) {
    reply_msg += '\n\n**However, they also cheated during this session!**'
  }

  try {
    await chan.send(reply_msg);
  } catch (error) {
    let botNoiseChannel = message.client.channels.cache.get(process.env.BOTNOISE_CHANNEL_ID);
    botNoiseChannel.send("An error occurred while sending a message to the intended channel. Here's the message content:\n\n" + reply_msg);
  }
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

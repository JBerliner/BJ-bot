import {clearShameUserRecord} from '../utils/shame.mjs';

const shameRoleName = process.env.SHAME_ROLE;
const logsChannelId = process.env.LOGS_ROOM;

export default (() => {
  return {
    name: 'guildMemberUpdate',
    async execute(oldMember, newMember) {
      const logsChannel = await newMember.guild.channels.fetch(logsChannelId);

      // Check if the user's roles were updated
      if (oldMember.roles.cache.size !== newMember.roles.cache.size) {
        // Check if user had the shame role
        const hasShameRole = oldMember.roles.cache.some((r) => r.name === shameRoleName);
        // Check if the user no longer has the shame role
        const removeShameRole = hasShameRole && !newMember.roles.cache.some((r) => r.name === shameRoleName);

        if (removeShameRole) {
          const message = `The role "${shameRoleName}" was removed from <@${newMember.id}>`;
          logsChannel.send(message);

          // Delete the brain key
          await clearShameUserRecord(newMember.id);
        }
      }
    },
  };
})();
